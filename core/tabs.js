var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");
var uify = require("./_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create a tabs display
  ARGUMENTS: ({
    !$container: <yquerjObject>,
    ?menuClass: <string> « class to apply to menus »,
    !tabs: <{
      !name: <string> « the name of this tab, must be unique, if you don't plan on using tabified.optn, you may omit it »,
      ?title: <string> « if not defined, will use name »,
      !content: <string|function($tabContentContainer)>,
      ?class: <string>,
    }[]>,
  })
  RETURN: <{
    uuid: <string>,
    options: <uifyTabsOptions>,
    tabs: <tab>,
    $content: <yquerjObject>,
    $menu: <yquerjObject>,
    $tabs: <yquerjObject>,
    $tabsTitles: <yquerjObject>,
    $tabsContents: <yquerjObject>,
    activeTab: <tab> « currently active tab »,
    open: <function(tabName<string>)> « open tab with this name »,
  TYPES:
    tab = <{
      name: <string> « tab name specified in options.tabs·entry »,
      uuid: <string> « uuid of this tab »,
      options: <options.tabs·entry>,
      $li: <yquerjObject> « the menu li element »,
      $title: <yquerjObject> « the title in $li »,
      $content: <yquerjObject> « the content of this tab »,
    }>
  }>
*/
uify.tabs = function (options) {

  var uuid = $$.uuid();

  //
  //                              CREATE TABS CONTAINERS

  var $content = options.$container.div({ class: "uify-tabs", });
  var $menu = $content.ul({ class: "uify-tabs-menu" });
  if (options.menuClass) $menu.addClass(options.menuClass);
  var $tabs = $content.div({ class: "uify-tabs-content" });

  //
  //                              CREATE TABS

  var $tabsTitles = $();
  var $tabsContents = $();
  var tabs = [];

  _.each(options.tabs, function (tabOptions, tabIndex) {

    var tab = {
      name: tabOptions.name,
      uuid: $$.uuid(),
      options: tabOptions,
    };

    // create tabs titles
    tab.$li = $menu.li();
    tab.$title = tab.$li.a({
      htmlSanitized: _.isUndefined(tab.options.title) ? tab.options.name : tab.options.title,
    });
    $tabsTitles = $tabsTitles.add(tab.$title);
    tab.$title.click(function(event) {
      event.preventDefault();
      tabified.open(tabs[tabIndex]);
    });

    // create tabs content
    tab.$content = $tabs.div({ class: "uify-tab-content", });
    if (tab.options.class) tab.$content.addClass(tab.options.class);
    if (_.isFunction(tab.options.content)) tab.options.content(tab.$content)
    else tab.$content.htmlSanitized(tab.options.content);
    $tabsContents = $tabsContents.add(tab.$content);

    tabs.push(tab);

  });

  // MAKE FIRST TAB ACTIVE
  tabs[0].$li.addClass("current");
  tabs[0].$content.show();

  //
  //                              MAKE TABIFIED OBJECT AND RETURN

  var tabified = {
    uuid: uuid,
    tabs: tabs,
    options: options,
    $content: $content,
    $menu: $menu,
    $tabs: $tabs,
    $tabsTitles: $tabsTitles,
    $tabsContents: $tabsContents,
  };
  tabified.open = _.bind(open, tabified);

  return tabified;

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  OPEN
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function open (tabOrTabName) {
  var tabified = this;

  if (_.isString(tabOrTabName)) var tab = _.findWhere(tabified.tabs, { name: tabOrTabName, })
  else var tab = tabOrTabName;

  // store currently active tab in tabified object
  tabified.activeTab = tab;

  // DO NOTHING IF CLICKING CURRENT TAB
  if (tab.$li.hasClass("current")) return

  // ELSE CHANGE TAB
  else {
    tab.$li.siblings().removeClass("current");
    tab.$li.addClass("current");
    tabified.$tabsContents.hide();
    // $(".tab-content").not(tab).css("display", "none");
    $(tab.$content).fadeIn();
  };

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = uify.tabs;

var _ = require("underscore");
var $$ = require("squeak");
var keyboardify = require("keyboardify");
var uify = require("../_");
var methods = require("./methods");
var spacesSwitcher = require("./switcher");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: allow to have multiple parallel spaces in a single dom element and to navigate between those spaces (with or without constraints)
  ARGUMENTS: ({
    !$target: <yquerjObject> «
      if this contains something, the content will be transfered to the first space (but don't rely on this, you're likely to lose things like events attached to dom elements... it uses $().html() method for copying)
      ⚠ the position of this will be set to relative
    »,
    ?hideRightArrow: <boolean>@default=false « if true, will not display right navigation arrow »,
    ?hideLeftArrow: <boolean>@default=false « if true, will not display left navigation arrow »,
    ?controlsPosition: <"top"|"middle"|"bottom"> « position of the arrows and/or pathBar (don't use middle with fullPathBar on) »,
    ?fullPathBar: <boolean> «
      if true, will show, a bar to navigate between spaces
      spaces id will be buttons to open the given space
      if a spaced doesn't have an id, it's index will be used
    »,
    ?pathDelimitor: <string> « any characters to display between spaces names in pathBar »,
    ?spaces: <spaceOptions[]> « a preset list of spaces to create on initialization »,
    ?animate: <boolean>@default=false « animate moving from one space to another »,
    ?visitCallback: <function(visitedSpace<spaceObject)> « executed when a space is visited »,
    ?removeCallback: <function(visitedSpace<spaceObject)> « executed when a space is removed »,
    ?switcher: <boolean|{
      ?title: <string> « an optional title to display on top of the switcher »,
      ?buttons: <uify.button·options[]> « some custom buttons to display in the switcher »,
      ?spaceFilter: <function(<spaceObject>):<boolean>> « if this is defined, will only show in switcher, spaces that pass this filter (the function returns a true like value for the space) »,
      ?alwaysShowThumbnails: <boolean>@default=false « if true, will show the img tag even if no thumbnail is defined for the space »,
    }>@default=false «
      if true or an object, will give the possibility to display a switcher showing all current spaces
      for now, you will have to make a button yourself and call it programmatically when you want it to be shown
    »,
  })
  RETURN: <{

    // general
    spaces: <spaceObject[]> « the list of displayed spaces »,
    currentIndex: <integer> « the index of currently shown space »,
    lastVisitedIndex: <integer|undefined> « the index of space show just before current one »,

    // main containers
    $el: <yquerjObject>,
    $spaces: <yquerjObject>,
    $spacesBar: <yquerjObject>,

    // spaceBar sub containers
    $pathBar: <yquerjObject>,
    $leftArrow: <yquerjObject>,
    $rightArrow: <yquerjObject>,
    $leftButtons: <yquerjObject>,
    $rightButtons: <yquerjObject>,

    // methods
    visit: <function(!spaceIdentifier)>,
    visitLastVisited: <function(!spaceIdentifier)> « visit last visited space »,
    create: <function(
      ?spaceOptions <spaceOptions>,
      ?silent <boolean> « if true, will not visit this space on creation »
    ):<spaceObject>> « add a new space »,
    next: <function(ø)> « visit next space »,
    previous: <function(ø)> « visit previous space »,
    remove: <function(?spaceIdentifier « if not defined, will remove last space »),
    removeAfter: <function(ø)> « remove all spaces after the current one »,
    get: <function(!spaceIdentifier)> « get a space in the list of existing space »,
    getIndex: <function(!spaceIdentifier)> « get the index of a space »,

    // internal methods
    updateContainerClass: <function(ø)> « INTERNAL update the classes of the container to show/hide arrows in consequence »
    refreshIconsInPathBar: <function(ø)> « INTERNAL »,
    setSelectedSpaceInPathBar: <function(ø)> « INTERNAL »,
    initializeSpacesBar: <function(ø)> « INTERNAL »,

    // switcher (those will only be present if the switcher option was set to true, or an object)
    switcher: <undefined|{
      show: <function(ø)> « show the spaces switcher »,
      hide: <function(ø)> « hide the spaces switcher »,
      makeButtons: <function(buttons<uify.button·options[]>):<void>> « make/remake buttons for the switcher »,
      buttons: <uify.buttons·return|undefined> « list of switcher's buttons »,
      actualize: <function(ø)> « actualize the list of spaces in the switcher (INTERNAL: ran automatically when list of spaces is modified) »
      ... see full API in uify/core/spaces/switcher.js, but the rest are desgined for internal usage
    }> « manipulate the switcher »,

  }>
  TYPES:
    spaceIdentifier <integer|string> «
      if string, must be the id of this space
      if integer should be the index of the space you want to remove
      NOTE: use negative numbers to start counting from the end, (but negative indexes start at -1)
    »
    spaceOptions = <{
      ?id: <string> « a unique identifier for this space »,
      ?name: <string> « a space name (used for pathBar) »,
      ?contentMaker: <function($container)>,
      ?spaceButtons: <uify.button·options[]> «
        list of buttons to display in path bar
        add postition <"left"|"right"> in each button to define on which side of the space bar it should be displayed (if not defined, defaults to "left")
      »,
      // ?position: <"before"|"after"|integer>@default="after" « where to add this new space in the list/order », // TODO: not yet implemented, could do it some day if useful, only if it's useful
      ?thumbnail: <string> « url of image to use as space thumbnail in space switcher »,
    }>
    spaceObject = <{
      spacified: <uify.spaces·return> « the spaces group this space belongs to »,
      id: <string> « unique identifier of this space »,
      $el: <yquerjObject>,
      options: <spaceOptions>,
      visit: <function(ø)> « navigate to this space »,
      remove: <function(ø) « remove this space »,
      getIndex: <function(ø)> « get the index of this space »,
      $switcherThumbnail: <yquerjObject> « the thumbnail in switcher (defined only if switcher is on) »,
    }>
*/
uify.spaces = function (options) {

  //
  //                              OPTIONS

  var defaultOptions = {
    onlyNavigateBack: false,
    controlsPosition: "middle",
    switcher: false,
  };
  options = $$.defaults(defaultOptions, options);

  //
  //                              CREATE SPACED OBJECT AND IT'S METHODS

  var spaced = {
    $el: options.$target,
    options: options,
    currentIndex: 0,
    spaces: [],
    // save keyboard context of initial creation of spaces, to reuse it for switcher refresh
    // doing it because otherwise, when switcher buttons are refreshed, if it's done from another keyboardify context, then switcher shortcuts change to that new context with is probably not intended
    keyboardifyContext: keyboardify.getContext(),
  };

  // add all methods to spaced object
  _.each(methods, function (func, key) {
    spaced[key] = _.bind(func, spaced);
  });

  //
  //                              ADD CLASS TO SPACES TARGET

  spaced.$el.addClass("uify-spaces displaying-first");
  spaced.$el.addClass("uify-spaces displaying-last");
  var targetContent = spaced.$el.html();
  spaced.$el.empty();

  //
  //                              CREATE SPACES STRUCTURE


  // spaces container and wrapper
  spaced.$spaces = spaced.$el.div({ class: "spaces", });
  spaced.$spacesWrapper = spaced.$spaces.div({ class: "spaces-wrapper", });
  if (spaced.options.animate) spaced.$spacesWrapper.addClass("animated");

  // create spaces bar containers
  spaced.initializeSpacesBar();

  //
  //                              CREATE FIRST SPACE IF THERE WAS SHIT ALREADY IN THE TARGET

  if (targetContent) {
    var firstSpace = spaced.create();
    firstSpace.$el.htmlSanitized(targetContent);
  };

  //
  //                              CREATE ASKED SPACES

  _.each(options.spaces, function (spaceOptions) {
    spaced.create(spaceOptions, true)
  });

  //
  //                              CREATE SPACES SWITCHER IF ASKED

  if (options.switcher) spaced.switcher = spacesSwitcher.initialize(spaced, options.switcher);

  //
  //                              RETURN SPACED OBJECT

  return spaced;

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = uify.spaces;

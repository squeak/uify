var _ = require("underscore");
var $$ = require("squeak");
var uify = require("../_");
require("../button");
var keyboardify = require("keyboardify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var switcherMethods = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INITIALIZE SWITCHER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: initialize the space switcher
    ARGUMENTS: (
      !spacified: <uify.spaces.return>,
      ?options: <see uify.spaces·options.switcher>,
    )
    RETURN: <switcher>
    TYPES:
      switcher = <{
        spacified: <uify.spaces·return>,
        options: <uify.spaces·options.switcher> « options passed to switcher »,
        $switcher <yquerjObject>,
        $currentSpaces <yquerjObject>,
        $additionalButtons <yquerjObject>,
        buttons <uify.buttons·return>,
        ... all keys from switcherMethods
      }>
  */
  initialize: function (spacified, options) {

    // CREATE SWITCHER OBJECT, ATTACH METHODS TO IT, AND SPACIFIED OBJECT
    var switcher = $$.methods({ spacified: spacified, }, switcherMethods);
    switcher.spacified = spacified;
    switcher.options = options;

    // MAKE BASE DOM ELEMENTS
    switcher.$switcher = spacified.$el.div({ class: "uify-spaces--switcher", }).click(function () { switcher.hide(); });
    switcher.$title = switcher.$switcher.div({ class: "title", });
    switcher.$currentSpaces = switcher.$switcher.div({ class: "current_spaces", });
    switcher.$additionalButtons = switcher.$switcher.div({ class: "additional_buttons", });

    // TITLE
    if (options.title) switcher.$title.htmlSanitized(options.title);

    // CURRENT SPACES
    if (spacified.spaces.length) switcher.actualize();

    // ADDITIONAL BUTTONS
    if (options.buttons) switcher.makeButtons(options.buttons);

    // RETURN SWITCHER OBJECT
    return switcher;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SHOW/HIDE SWITCHER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  show: function () {
    var switcher = this;
    // make sure to refresh buttons in the appropriate keyboardify context (and also to add esc binding)
    keyboardify.withContext(switcher.spacified.keyboardifyContext, function () {
      // refresh buttons every time showing the switcher, in case their state has changed while they were hidden
      _.each(switcher.buttons, function (button) { button.refresh(); });
      // setup "esc" shortcut to close it
      keyboardify.bind("esc", switcher.hide);
    });
    // display switcher
    switcher.$switcher.addClass("visible");
  },

  hide: function () {
    var switcher = this;
    // hide switcher
    switcher.$switcher.removeClass("visible");
    // make sure to unbind esc in the appropriate keyboardify context
    keyboardify.withContext(switcher.spacified.keyboardifyContext, function () {
      // unset "esc" shortcut
      keyboardify.unbind("esc", switcher.hide);
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ACTUALIZE CURRENT SPACES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: actualizes the list of current spaces
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  actualize: function () {
    var switcher = this;

    // REMOVE PREVIOUS THUMBNAILS BEFORE REFRESHING
    switcher.$currentSpaces.empty();

    // MAKE SPACES THUMBNAILS
    _.each(switcher.spacified.spaces, function (spaceObject) {

      // do not create thumnail for this space if filtered out
      if (switcher.options.spaceFilter && !switcher.options.spaceFilter(spaceObject)) return;

      // make clickable thumbnail
      spaceObject.$switcherThumbnail = switcher.$currentSpaces.div({ class: "space_thumbnail", }).click(function (e) {
        e.stopPropagation();
        spaceObject.visit();
        switcher.hide();
      });

      // display space image
      if (spaceObject.options.thumbnail || switcher.options.alwaysShowThumbnails) spaceObject.$switcherThumbnail.div({ class: "image", }).css("background-image", 'url("'+ spaceObject.options.thumbnail +'")');

      // make space name
      spaceObject.$switcherThumbnail.div({ class: "title", htmlSanitized: spaceObject.name || spaceObject.id, });

      // make space closing cross
      if (switcher.spacified.spaces.length > 1) uify.button({
        $container: spaceObject.$switcherThumbnail,
        icomoon: "cross", // cancel-circle
        click: function (e) {
          e.stopPropagation();
          spaceObject.remove();
        },
      });

    });

    // ACTUALIZE WHICH THUMBNAIL IS CURRENT
    switcher.actualizeCurrent();

  },

  /**
    DESCRIPTION: actualize which thumbnail should have the "current" class, because it's the one of the currently shown space
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  actualizeCurrent: function () {
    var switcher = this;
    switcher.$currentSpaces.find(".current").removeClass("current");
    var currentSpace = switcher.spacified.getCurrent();
    if (currentSpace && currentSpace.$switcherThumbnail) currentSpace.$switcherThumbnail.addClass("current");
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ADD ADITIONAL BUTTONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: make/remake additional buttons
    ARGUMENTS: ( !buttons <uify.button·options[]> )
    RETURN: <void>
  */
  makeButtons: function (buttons) {
    var switcher = this;

    // in case of remaking, remove previous buttons
    switcher.$additionalButtons.empty();

    // buttons clicking should hide switcher
    var finalButtons = _.map(buttons, function (butt) {
      var button = _.clone(butt);
      var previousClick = button.click;
      button.click = function (e) {
        e.stopPropagation();
        if (previousClick) previousClick.call(this, e);
        switcher.hide();
      };
      return button;
    });

    // make buttons
    switcher.buttons = uify.buttons(switcher.$additionalButtons, finalButtons);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = switcherMethods;

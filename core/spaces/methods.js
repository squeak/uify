var _ = require("underscore");
var $$ = require("squeak");
var uify = require("../_");
require("../button");

// all methods in this file will be attached to the spaced object and bound to it when executed
module.exports = {
  //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
  //                                                  GENERAL
  //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CREATE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  create: function (spaceOptions, silent) {
    var spaced = this;

    //
    //                              SPACE OPTIONS

    var defaultSpaceOptions = {
      id: $$.uuid(),
    };
    spaceOptions = $$.defaults(defaultSpaceOptions, spaceOptions);

    //
    //                              CREATE SPACE OBJECT AND CONTENT IF ASKED

    var space = {
      spacified: spaced,
      id: spaceOptions.id,
      options: spaceOptions,
      $el: spaced.$spacesWrapper.div({
        class: "space",
      }),
      visit: function () { spaced.visit(space.id); },
      remove: function () { spaced.remove(space.id); },
      getIndex: function () {
        return _.findIndex(spaced.spaces, function (sp) { return sp.id == space.id });
      },
      positionize: function (position) {
        space.$el.css("left", "calc(100% * "+ position +")");
      },
    };

    // create content if asked
    if (spaceOptions.contentMaker) spaceOptions.contentMaker(space.$el);

    //
    //                              ADD SPACE TO LIST, VISIT IT, RETURN IT

    space.positionize(spaced.spaces.length); // TODO: if position customized below, maybe this should be adapted
    spaced.spaces.push(space); // TODO: could customize and add the possibility to add space at other position

    // visit newly created space
    if (!silent) space.visit();

    // refresh pathbar
    spaced.refreshPathBar();

    // update class of container
    spaced.updateContainerClass();

    // update switcher
    if (spaced.switcher) spaced.switcher.actualize();

    // return space object
    return space;

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REMOVE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  remove: function (spaceIdentifier) {
    var spaced = this;

    // if no spaceIdentifier defined, do it on last space
    if (_.isUndefined(spaceIdentifier)) spaceIdentifier = spaced.spaces.length - 1;

    // get space and index to remove
    var space = spaced.get(spaceIdentifier);
    var spaceIndex = spaced.getIndex(spaceIdentifier);

    // remove space in dom and from list
    space.$el.remove();
    spaced.spaces.splice(spaceIndex, 1);
    for (var i = spaceIndex; i < spaced.spaces.length; i++) {
      spaced.spaces[i].positionize(i);
    };
    spaced.updateContainerClass();

    // refresh pathbar
    spaced.refreshPathBar();

    // change position if removed space is current
    if (spaceIndex <= spaced.currentIndex) spaced.previous();

    // update switcher
    if (spaced.switcher) spaced.switcher.actualize();

    // optional callback
    if (spaced.options.removeCallback) spaced.options.removeCallback(space);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REMOVE AFTER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  removeAfter: function () {
    var spaced = this;

    // remove spaces starting from end until index just after the current one
    for (var i = spaced.spaces.length -1; i > spaced.currentIndex; i--) {
      spaced.remove(i);
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  UPDATE CONTAINER CLASS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  updateContainerClass: function () {
    var spaced = this;
    spaced.$el.removeClass("displaying-first displaying-last");
    if (spaced.currentIndex == 0) spaced.$el.addClass("displaying-first");
    if (spaced.currentIndex == spaced.spaces.length -1) spaced.$el.addClass("displaying-last");
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  VISIT, NEXT, PREVIOUS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  visit: function (spaceIdentifier) {
    var spaced = this;

    // set previously visited space index
    spaced.lastVisitedIndex = spaced.currentIndex;

    // get space and it's index
    var spaceIndex = spaced.getIndex(spaceIdentifier);

    // position spaces container appropriately
    spaced.currentIndex = spaceIndex;
    spaced.$spacesWrapper.css("left", "calc(100% * -"+ spaceIndex +")");

    // update class of container
    spaced.updateContainerClass();

    // set this space current class
    var spaceObject = spaced.getCurrent();
    _.each(spaced.spaces, function (spaceObjectIterated) { spaceObjectIterated.$el.removeClass("current"); });
    spaceObject.$el.addClass("current");

    // set which spaced is selected in pathbar
    spaced.setSelectedSpaceInPathBar();

    // show icons for this space in path bar
    spaced.refreshIconsInSpacesBar();

    // actualize which thumbnail is current in spaces switcher
    if (spaced.switcher) spaced.switcher.actualizeCurrent();

    // execute visit callback if one is defined
    if (spaced.options.visitCallback) spaced.options.visitCallback(spaced.get(spaced.currentIndex));

  },

  next: function () {
    var spaced = this;
    if (spaced.currentIndex < spaced.spaces.length - 1) spaced.visit(spaced.currentIndex + 1);
  },

  previous: function () {
    var spaced = this;
    if (spaced.currentIndex > 0) spaced.visit(spaced.currentIndex - 1);
  },

  visitLastVisited: function () {
    var spaced = this;
    if (!_.isUndefined(spaced.lastVisitedIndex)) spaced.visit(spaced.lastVisitedIndex);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET SPACE INDEX
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  getIndex: function (spaceIdentifier) {
    var spaced = this;

    if (_.isNumber(spaceIdentifier)) return spaceIdentifier
    else {
      var space = spaced.get(spaceIdentifier);
      if (space) return space.getIndex()
      else return -1;
    };

  },


  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET SPACE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  get: function (spaceIdentifier) {
    var spaced = this;
    if (_.isNumber(spaceIdentifier)) return spaced.spaces[spaceIdentifier]
    else return _.findWhere(spaced.spaces, { id: spaceIdentifier, });
  },

  getCurrent: function () {
    var spaced = this;
    return spaced.get(spaced.currentIndex);
  },

  //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
  //                                                  SPACES BAR
  //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INITIALIZE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  initializeSpacesBar: function () {
    var spaced = this;

    // create spaces bar
    spaced.$spacesBar = spaced.$el.div({
      class: "spaces_bar "+ spaced.options.controlsPosition,
    });

    //
    //                              left arrow

    spaced.$leftArrow = spaced.$spacesBar.div({
      class: "arrow arrow-left icon-arrow-left3",
    }).click(function () {
      spaced.previous();
    });
    if (spaced.options.hideLeftArrow) spaced.$leftArrow.addClass("hidden");

    //
    //                              left buttons container

    spaced.$leftButtons = spaced.$spacesBar.div({
      class: "space_bar_buttons space_bar_buttons-left",
    });

    //
    //                              path bar

    spaced.$pathBar = spaced.$spacesBar.div({ class: "path_bar", });

    //
    //                              left buttons container

    spaced.$rightButtons = spaced.$spacesBar.div({
      class: "space_bar_buttons space_bar_buttons-left",
    });

    //
    //                              right arrow

    spaced.$rightArrow = spaced.$spacesBar.div({
      class: "arrow arrow-right icon-arrow-right3",
    }).click(function () {
      spaced.next();
    });
    if (spaced.options.hideRightArrow) spaced.$rightArrow.addClass("hidden");

    // if full path bar asked, make content of path bar and set that it's made in it's container
    // do this after all containers have been created
    if (spaced.options.fullPathBar) spaced.refreshPathBar();

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE/REFRESH PATH BAR
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  refreshPathBar: function () {
    var spaced = this;

    // if no fullPathBar just do nothing
    if (!spaced.options.fullPathBar) return;

    // make sure main elemen has the appropriate classes
    if (spaced.spaces.length > 1) spaced.$el.addClass("has_full_path_bar has_full_path_bar-"+ spaced.options.controlsPosition)
    else spaced.$el.removeClass("has_full_path_bar has_full_path_bar-"+ spaced.options.controlsPosition);

    // remove previous content of path bar
    spaced.$pathBar.empty();

    // for each space
    _.each(spaced.spaces, function (space, i) {

      // create space button
      var $butt = spaced.$pathBar.div({
        class: "path_part path_part-"+ i,
        text: !_.isUndefined(space.options.name) ? space.options.name : space.id,
      }).click(function () {
        spaced.visit(i);
      });

      // if not last, add delimitor
      if (spaced.options.pathDelimitor && i+1 != spaced.spaces.length) spaced.$pathBar.div({
        class: "path_delimitor",
        text: spaced.options.pathDelimitor,
      });

    });

    // set which spaced is selected in pathbar
    spaced.setSelectedSpaceInPathBar();

    // show icons for the current space in path bar
    spaced.refreshIconsInSpacesBar();

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SHOW ICONS FOR CURRENT SPACE IN PATH BAR
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  refreshIconsInSpacesBar: function () {
    var spaced = this;

    // empty buttons containers
    spaced.$leftButtons.empty();
    spaced.$rightButtons.empty();

    // get current space
    var currentSpace = spaced.get(spaced.currentIndex);

    // display buttons for current space
    if (currentSpace) _.each(currentSpace.options.spaceButtons, function (buttonOptions) {
      buttonOptions.$container = buttonOptions.position == "left" ? spaced.$leftButtons : spaced.$rightButtons;
      uify.button(buttonOptions);
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SET SELECTED SPACE IN PATH BAR
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  setSelectedSpaceInPathBar: function () {
    var spaced = this;

    spaced.$pathBar.find(".path_part.selected").removeClass("selected");
    spaced.$pathBar.find(".path_part-"+ spaced.currentIndex).addClass("selected");

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
};

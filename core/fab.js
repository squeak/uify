var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/dom");
var uify = require("./_");
require("./button");
var setPositionsAndShape = require("./fab.position");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display a fab button
  ARGUMENTS: ({
    !$container: <jqueryObject>,
    ?size: <number>@default=50 « fab's size »,
    ?space: <number>@default=20 « fab's spacing with border »,
    ?direction: <"vertical"|"horizontal">@default="vertical",
    ?position: <{
      ["right"|"left"|"top"|"bottom"|"middle"|"center"]: <"default"|"stick"|number>
    }> «
      example: { top: "default", right: 20 }
      "stick" will make it a half circle stuck to the border (cannot be used for middle or center),
      "default": will set it at the default distance to the specified side,
      number: will set it at the specified distance to the specified side,
    »,
    ?color: <string>,
    ?colorHover: <string>,
    ... any option that uify.button can accept
  })
  RETURN: <see uify.button·return>
  NOTE: if you use toolbarify, you must create fabs after toolbars for the middle and center position to be exactly respected (without counting toolbar width in the measurement of center)
*/
uify.fab = $$.scopeFunction({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  main: function (givenOptions) {

    //
    //                              GET ALL OPTIONS

    var options = getAllOptions(givenOptions);

    //
    //                              CREATE BUTTON

    var button = uify.button(options);
    var positionCodeClass = "uify-fab-"+ JSON.stringify(options.position).replace(/"/g, "").replace(/{/g, "").replace(/}/g, "").replace(/,/g, "-").replace(/:/g, "_");
    button.$button.addClass("uify-fab "+ positionCodeClass);

    // attach fab options to button
    button.fabOptions = options;

    //
    //                              CUSTOM COLOR

    if (options.color) {
      button.$button.css("background-color", options.color);
      if (options.colorHover) button.$button.hover(
        function () { button.$button.css("background-color", options.colorHover); },
        function () { button.$button.css("background-color", options.color);
      });
    };

    //
    //                              SET POSITION AND SHAPE, AND IT'S REFRESHING ON WINDOW RESIZE

    function getDimensionOfOtherFabsInSamePosition () {
      var fabsInSamePosition = options.$container.find("."+ positionCodeClass);
      var dimension = _.reduce(fabsInSamePosition, function (memo, buttonElement) {
        var button = $(buttonElement).data().uifyButton;
        return memo + button.fabOptions.size;
      }, 0);
      return dimension - options.size;
    };
    var dimensionOfOtherFabsInSamePosition = getDimensionOfOtherFabsInSamePosition();
    setPositionsAndShape(button, dimensionOfOtherFabsInSamePosition);
    $$.dom.onWindowResize(function () { setPositionsAndShape(button, dimensionOfOtherFabsInSamePosition); });

    //
    //                              RETURN

    return button;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  OPTIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function getAllOptions (options) {

  // wrong type for options
  if (!$$.isObjectLiteral(options)) $$.log.detailedError("uify.fab/getAllOptions", "options should be an object");

  var defaultOptions = {
    size: 50,
    space: 20,
    position: { right: "default", bottom: "default", },
    direction: "vertical",
    css: {
      width: function () { return options.size; },
      height: function () { return options.size; },
      fontSize: function () { return options.size / 2; },
      padding: function () { return options.size / 4; },
      borderRadius: function () { return options.size; },
    },
  };
  options = $$.defaults(defaultOptions, options);

  // wrong type for options.position
  if (!$$.isObjectLiteral(options.position)) $$.log.detailedError("uify.fab/getAllOptions", 'options.position should be an object in the format: { ["right"|"left"|"top"|"bottom"|"middle"|"center"]: <"default"|"stick"|number> }');

  return options;

}
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = uify.fab;

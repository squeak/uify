var _ = require("underscore");
var $$ = require("squeak");
var uify = require("./_");
require("./dialog");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: make a simple alert with uify.dialog
  ARGUMENTS: (
    !optionsOrContent <{
      ?title: <string|function(ø):<string>> « title of the dialog (in top menubar) »,
      ?subtitle: <string|function(ø):<string>> « subtitle of the dialog (in top menubar) »,
      ?content: <string|function($contentContainer<yquerjObject>, options<uify.confirm·options>)>,
      ... all uify.dialog options except buttons, menuBar.buttons, ok and cancel
    }|string> « if it's a string, will just use it to define content »,
    ?type <string> « if defined, will be passed as type to options (convenient when you set a content string as first argument) »,
  )
  RETURN: <uify.dialog·return>
*/
uify.alert = $$.scopeFunction({

  /**
    see uify.alert
  */
  main: function (settings, type) {

    //
    //                              CUSTOMIZE SETTINGS FOR uify.dialog

    if (!settings) settings = {};
    if (_.isString(settings) || _.isFunction(settings)) settings = { content: settings, };
    if (type) settings.type = type;

    // only ok button
    settings.buttons = [ "ok" ];
    // setup cancel like ok (because if esc is hit, it's cancel function that is called)
    if (!settings.cancel) settings.cancel = settings.ok;

    //
    //                              CREATE DIALOG AND RETURN

    dialog = uify.dialog(settings);

    // add uify-alert class
    dialog.$dialog.addClass("uify-alert");

    return dialog;

    //                              ¬
    //

  },

  /**
    DESCRIPTION: display a success alert
    ARGUMENTS: ( see uify.alert options )
    RETURN: <uify.dialog·return>
  */
  success: function (settings) { return uify.alert(settings, "success"); },

  /**
    DESCRIPTION: display a warning alert
    ARGUMENTS: ( see uify.alert options )
    RETURN: <uify.dialog·return>
  */
  warning: function (settings) { return uify.alert(settings, "warning"); },

  /**
    DESCRIPTION: display an error alert
    ARGUMENTS: ( see uify.alert options )
    RETURN: <uify.dialog·return>
  */
  error:   function (settings) { return uify.alert(settings, "error");   },

});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = uify.alert;

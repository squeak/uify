var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/string");
var $ = require("yquerj");
var uify = require("./_");
require("./button");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  ADD FIELD
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create an item field
  ARGUMENTS: (
    $item,
    customFieldOptions,
  )
  RETURN: <{
    $el: <yquerjObject>,
    options: <uify.item:options>,
    buttons: <uify.button:return>,
  }>
*/
function makeField ($item, options) {

  var $field = $item[options.tag || "div"]({ class: "uify-item-field", });
  var htmlSanitized = $$.string.make($$.result(options.html, $field), { htmlifyObjects: true, });
  if (htmlSanitized) $field.htmlSanitized(htmlSanitized);

  if (options.class) $field.addClass(options.class);
  if (options.click) $field.click(options.click);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE ITEM
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeItemContents (item) {
  var options = item.options;

  //
  //                              AVATAR

  if (options.avatar || options.avatarImage) {
    var $avatar = item.$el.div({ class: "uify-item-avatar", });
    if (options.avatarImage) uify.button({ $container: $avatar, icon: options.avatarImage, css: { cursor: "default" } })
    else if (_.isObject(options.avatar)) uify.button(options.avatar, $avatar)
    else uify.button({ $container: $avatar, icomoon: options.avatar, css: { cursor: "default" }, });
  };

  //
  //                              FIELDS

  var $fields = item.$el.div({
    class: "uify-item-fields",
  });

  // STANDARD FIELDS
  if (options.label) makeField($fields, { class:"uify-item-field-label", html: options.label, tag: "label", });
  if (options.title) makeField($fields, { class:"uify-item-field-title", html: options.title, });
  if (options.text) makeField($fields, { class:"uify-item-field-text", html: options.text, });
  if (options.comment) makeField($fields, { class:"uify-item-field-comment", html: options.comment, });

  // CUSTOM FIELDS
  _.each(options.customFields, function (fieldParameters) {
    return makeField($fields, fieldParameters);
  });

  //
  //                              CLASS, CLICK...

  if (options.class) item.$el.addClass(options.class);
  if (options.click) {
    $fields.click(options.click);
    $fields.css("cursor", "pointer");
  };

  //
  //                              BUTTONS


  item.$buttons = item.$el.div({ class: "uify-item-buttons", });
  item.$buttons.css("flex-direction", options.buttonsOrientation);

  var buttonsOptions = options.buttons ? options.buttons : (options.button ? [options.button] : []);
  if (options.buttonsContext) buttonsOptions = _.map(buttonsOptions, function (buttonOptions) {
    buttonOptions = _.clone(buttonOptions);
    if (!buttonOptions.context) buttonOptions.context = options.buttonsContext;
    return buttonOptions;
  });

  item.buttons = uify.buttons(item.$buttons, buttonsOptions);

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display an item in a list (a line with content and buttons)
  ARGUMENTS: ({

    !$container: <yquerjProcessable>,

    // fields (everything is optional)
    ?text: <string|function($container):<string>> « normal size »,
    ?label: <string|function($container):<string>> « secondary color »,
    ?title: <string|function($container):<string>> « bigger »,
    ?comment: <string|function($container):<string>> « smaller »,
    ?customFields: <customFieldOptions[]>,

    // styling
    ?tag: <string> « tag used for this item, if not specified default is "div" »,
    ?class: <string>,

    // buttons
    ?avatar: <string|uify.button·options> « icomoon string, or custom button options (imageAvatar has priority over this option) »,
    ?avatarImage: <string> « if this is defined, will not use avatar option, but display the given image url instead »,
    ?button: <uify.button·options>,
    ?buttons: <uify.button·options[]>,
    ?buttonsOrientation: <"row"|"column"> « default="row" »,
    ?buttonsContext: <any> « a custom context to pass to buttons (this will be passed to all buttons click events callbacks) »,

    // events
    ?click: <function(ev)>,

  })
  RETURN: <{
    $el: <yquerjObject>,
    options: <uify.item·options>,
    buttons: <uify.button·return[]>,
    removeButtons: <function(ø)> « remove all item buttons »,
    addButtons: <function(buttons<uify.button·options[]>) « add buttons to item »,
    remake: <function(ø)> « recalculate item display »,
  }>
  TYPES:
    customFieldOptions = {
      ?class: <string> « custom class to apply to this field »,
      ?tag: <string> « tag used for this field, if not specified default is "div" »,
      ?html: « html content of this field »,
      ?click: <function(ev)> « ac action to executed when this element is clicked »,
    }
*/
uify.item = function (options) {

  //
  //                              DEFAULTS

  var defaultOptions = {
    tag: "div",
    buttonsOrientation: "row",
  };
  options = $$.defaults(defaultOptions, options);

  //
  //                              MAKE ITEM OBJECT

  var item = {

    // item options
    options: options,

    // item element
    $el: $(options.$container)[options.tag]({ class: "uify-item", }),

    // remake item
    remake: function (newOptions) {
      item.options = $$.defaults(item.options, newOptions);
      item.$el.empty();
      makeItemContents(item);
    },

    // buttons-related methods
    removeButtons: function () {
      item.$buttons.empty();
      item.buttons = [];
    },
    addButtons: function (buttons) {
      _.each(buttons, function (buttonParameters) {
        item.buttons.push(uify.button(buttonParameters, item.$buttons));
      });
    },

  };

  //
  //                              MAKE ITEM CONTENTS

  makeItemContents(item);

  //
  //                              RETURN ITEM OBJECT

  return item;

  //                              ¬
  //

}

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = uify.item;

var _ = require("underscore");
var $$ = require("squeak");
var uify = require("./_");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: display a progress bar that can be updated along the process that's taking place
  ARGUMENTS: ({
    ?$container: <yquerjProcessable>,
    ?title: <string|function($container)>,
    ?steps: <integer>@default=100 « the total number of steps that will be achieved »,
    ?style: <"dialog"|"toast">@default="dialog",
    ?class: <string> « class to apply to the dialog »,
  })
  RETURN: <{
    options: <uify.progress·options>,
    count: <integer> « the current amount of steps that have been achieved »,
    refresh: <function(index <integer> « how many steps have been made since last refresh »)>,
    destroy: <function(interrupt <boolean> « set this to true if should not display success animation when destroying the progress bar »)>,
  }>
*/
uify.progress = function (options) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FULL OPTIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  var defaultOptions = {
    $container: $("body"),
    steps: 100,
    style: "dialog",
  };
  options = $$.defaults(defaultOptions, options);
  options.$container = $(options.$container);
  options.class = options.class ? "uify-progress "+ options.class : "uify-progress";

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE PROGRESS OBJECT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  var progressified = {
    options: options,
    count: 0,
    setDisplay: function (newCount) {
      progressified.$done.css("width", (100*newCount/options.steps) +"%");
      progressified.$text.htmlSanitized(newCount +"/"+ options.steps);
    },
    refresh: function (stepsMade) {
      if (_.isUndefined(stepsMade)) stepsMade = 1;
      progressified.count += stepsMade;
      progressified.setDisplay(progressified.count);
      if (progressified.count >= options.steps) progressified.destroy();
    },
    destroy: function (interrupt) {
      if (!interrupt) progressified.setDisplay(progressified.options.steps);
      setTimeout(function () {
        progressified.dialog.destroy();
        delete progressified;
      }, interrupt ? 0 : 500);
    },
  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE BAR
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  function makeBar ($container) {
    progressified.$bar = $container.div({ class: "bar", });
    progressified.$done = progressified.$bar.div({ class: "done", });
    progressified.$text = progressified.$bar.div({ class: "text", }).span();
  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY PROGRESS DIALOG OR TOAST
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  if (options.style == "toast") progressified.dialog = uify.toast({
    title: options.title,
    message: makeBar,
    duration: 0,
    class: options.class,
    dismissOnClick: false,
  })

  else progressified.dialog = uify.dialog({
    title: options.title,
    content: makeBar,
    notClosable: true,
    buttons: [],
    dialogClass: options.class,
  });

  progressified.refresh(0);

  return progressified;

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = uify.progress;

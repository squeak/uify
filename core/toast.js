var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/date");
require("squeak/extension/color");
require("squeak/extension/storage");
var $ = require("yquerj");
var uify = require("./_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GET TOAST OPTIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function getToastOptions (messageOrOptions) {
  var passedToastOptions = _.isString(messageOrOptions) ? { message: messageOrOptions, } : messageOrOptions;
  return $$.defaults(uify.toast.defaultOptions, passedToastOptions);
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  UPDATE TOAST POSITIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function updateToastsPositions () {
  var allVisibleToasts_fromLastToFirst = $(".uify-toast-visible").toArray().reverse();
  var nextToastPosition = 0;
  _.each(allVisibleToasts_fromLastToFirst, function (toastElement) {
    // set toast position
    $(toastElement).css("bottom", nextToastPosition);
    // add height of current toast (+10) to nextToastPosition value
    nextToastPosition += +$(toastElement).css("height").replace(/px$/, "") + 10;
  });
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DISPLAY TOAST
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var $toastsContainer;
var toastsHistory = $$.storage.local("uify-toasts-history") || [];
function displayToast (options) {

  // create toasts container if it doesn't exist already
  if (!$toastsContainer) $toastsContainer = $("body").div({ class: "uify-toast-container", });

  // create toast object
  var uifyToast = {
    $container: $toastsContainer,
    $el: $toastsContainer.div({ class: "uify-toast", }),
    options: options,
    destroy: function () {

      // if it has already been destroyed
      if (!uifyToast) return;

      // hide toast
      uifyToast.$el.removeClass("uify-toast-visible");

      // remove toast
      setTimeout(function () {
        uifyToast.$el.remove();
        delete uifyToast;
      }, 1000);

      // update positions of all toasts
      updateToastsPositions();

    },
  };

  // wait a little bit to add "visible" class otherwise animation doesn't play
  setTimeout(function () {

    uifyToast.$el.addClass("uify-toast-visible");

    // move previous toasts
    updateToastsPositions();

  }, 100);

  // icon
  if (options.icon) uifyToast.$icon = uifyToast.$el.div({ class: "uify-toast-icon icon-"+ $$.result(options.icon), });

  // title and content container
  uifyToast.$contentContainer = uifyToast.$el.div({ class: "uify-toast-content_container", });

  // title
  if (options.title) uifyToast.$title = uifyToast.$contentContainer.div({ class: "uify-toast-title", });
  if (_.isFunction(options.title)) options.title(uify.toast.$title)
  else if (options.title) uifyToast.$title.htmlSanitized(options.title);

  // content
  uifyToast.$content = uifyToast.$contentContainer.div({ class: "uify-toast-message", });
  if (_.isFunction(options.message)) var toastMessage = options.message(uifyToast.$content)
  else {
    uifyToast.$content.htmlSanitized(options.message);
    var toastMessage = options.message;
  };

  // eventual button
  if (options.button) {
    var toastButton = uify.button(options.button, uifyToast.$content);
    toastButton.$button.addClass("uify-toast-button");
  };

  // type, class and color options
  if (options.type) uifyToast.$el.addClass(options.type);
  if (options.class) uifyToast.$el.addClass(options.class);
  if (options.color) uifyToast.$el.css($$.color.contrastStyle(options.color));

  // duration
  if (options.duration) setTimeout(function () { uifyToast.destroy(); }, options.duration * 1000);

  // dismiss on click
  if (options.dismissOnClick) uifyToast.$el.click(function () { uifyToast.destroy(); });

  // save toast to history
  if (options.historify !== false) {
    toastsHistory.unshift({
      category: options.historify,
      message: toastMessage,
      options: options,
      date: $$.date.moment().format("YYYY-MM-DD_HH:mm:ss"),
    });
    $$.storage.local("uify-toasts-history", toastsHistory);
  };

  // return toast object
  return uifyToast;

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: display a popup toast message in the bottom right of the page
  ARGUMENTS: (
    !messageOrOptions <string|toastOptions> «
      if this is a string, will display toast with this message and default options
      else will use this as options
    »,
  )
  RETURN: <{
    $container: <yquerjObject>,
    $el: <yquerjObject>,
    $title: <yquerjObject>,
    $content: <yquerjObject>,
    options: <toastOptions>,
    destroy: <function(ø)> « remove this toast »,
  }>
  TYPE:
    toastOptions = <{
      !message: <string|function($container)> «
        a message, or a function so you can create yourself the content of the message
        if you care about toasts history and pass a function here, the function should also return a message value to save in history
      »,
      ?title: <string|function($container)> « a message, or a function so you can create yourself the title of the toast »,
      ?type: <"success"|"warning"|"error"> « use a predefined style of toast »,
      ?color: <string> « custom color to apply to the toast »,
      ?icon: <string|function(ø)> « an icon to display at left of toast text »,
      ?class: <string> « custom class to apply to the toast element »,
      ?duration: <number>@default=5 « how long the toast will be shown (in seconds), if 0 will stay until clicked »,
      ?button: <uify.button·options> « an evenutal button to display on the toast message »,
      ?dismissOnClick: <boolean>@default=true « if false, clicking the toast will not dismiss it »,
      ?historify: <string>@default="general" «
        category to use to save this toast to history
        if you don't want to save this toast to history, pass false here
      »,
    }>
*/
uify.toast = $$.scopeFunction({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DEFAULT OPTIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: global default options for toasts
    TYPE: <uify.toast·options>
  */
  defaultOptions: {
    duration: 5,
    dismissOnClick: true,
    historify: "general",
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAIN
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  main: function (messageOrOptions) {
    return displayToast(getToastOptions(messageOrOptions));
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SUCCESS, WARNING, ERROR
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display a toast with type "success"
    ARGUMENTS: ( see uify.toast·messageOrOptions )
    RETURN: <see uify.toast·return>
  */
  success: function (messageOrOptions) {
    var fullToastOptions = getToastOptions(messageOrOptions);
    fullToastOptions.type = "success";
    return displayToast(fullToastOptions);
  },

  /**
    DESCRIPTION: display a toast with type "warning"
    ARGUMENTS: ( see uify.toast·messageOrOptions )
    RETURN: <see uify.toast·return>
  */
  warning: function (messageOrOptions) {
    var fullToastOptions = getToastOptions(messageOrOptions);
    fullToastOptions.type = "warning";
    return displayToast(fullToastOptions);
  },

  /**
    DESCRIPTION: display a toast with type "error"
    ARGUMENTS: ( same as uify.toast·messageOrOptions, except that default duration is 10 )
    RETURN: <see uify.toast·return>
  */
  error: function (messageOrOptions) {
    var fullToastOptions = getToastOptions(messageOrOptions);
    fullToastOptions.type = "error";
    if (!_.isObject(messageOrOptions) || _.isUndefined(messageOrOptions.duration)) fullToastOptions.duration = 10;
    return displayToast(fullToastOptions);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DESKTOP NOTIFICATIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: send both toast and desktop notification
    ARGUMENTS: ( see uify.toast·messageOrOptions only difference is that duration is 0 by default )
    RETURN: <see uify.toast·return>
  */
  desktop: function (messageOrOptions) {

    var fullToastOptions = getToastOptions(messageOrOptions);
    if (!messageOrOptions || !messageOrOptions.duration) fullToastOptions.duration = 0;

    // request permission to send desktop notification (if not already granted)
    uify.toast.requestDesktopPermission().then(function () {
      // send notification
      if (fullToastOptions.title) new Notification(fullToastOptions.title, { body: fullToastOptions.message })
      else new Notification(fullToastOptions.message);
    }).catch($$.log.error);

    // in all case, send a toast of the notification
    return uify.toast(fullToastOptions);

  },

  /**
    DESCRIPTION:
      request permission from user to send desktop notifications,
      will resolve only if it's possible to send notifications to the user
    ARGUMENTS: ( ø )
    RETURN: <Promise.then(ø).catch(reason<"unsupported"|"denied"|"other">)>
  */
  requestDesktopPermission: function () {

    return new Promise(function (resolve, reject) {

      if (!window.Notification) reject("unsupported")
      else {
        if (Notification.permission === "granted") resolve()
        else {
          // request permission from user
          Notification.requestPermission().then(function (p) {
            if (p === "granted") resolve()
            else reject("denied");
          }).catch(function (err) {
            reject("other");
            $$.log.error(err);
          });
        };
      };

    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  TOASTS HISTORY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get history of shown toasts for the given or all categories
    ARGUMENTS: (
      ?category <string> «
        the category of toasts you want history of
        if undefined, will return toasts of all categories
      »,
    )
    RETURN: <{
      category: <string> « historify category for this toast »,
      message: <any> « the message this toast displayed »,
      options: <toastOptions> « options passed for this toast creation »,
    }[]>
  */
  history: function (category) {
    if (category) return _.where(toastsHistory, { category: category, })
    else return toastsHistory;
  },

  /**
    DESCRIPTION: clear history of shown toasts for the given or all categories
    ARGUMENTS: (
      ?category <string> «
        the category of toasts you want to clear history of
        if undefined, will clear history for all categories of toasts
      »,
    )
    RETURN: <void>
  */
  clearHistory: function (category) {
    if (category) toastsHistory = _.reject(toastsHistory, function (toast) { return toast.category === category; })
    else toastsHistory = [];
    $$.storage.local("uify-toasts-history", toastsHistory);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = uify.toast;

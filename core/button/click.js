var _ = require("underscore");
var $$ = require("squeak");
var uify = require("../_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (button, options) {

  // CLICK SELECT/UNSELECT
  if (options.clickSelect || options.clickUnselect) return function (ev) {
    if (!button.clicked) {
      // click
      if (options.clickSelect) options.clickSelect.call(options.context, ev);
      // set clicked display
      button.setDisplay.clicked();
      button.clicked = true;
      // if asked, save status
      if (options.clickedLocalStorageId) $$.storage.local(options.clickedLocalStorageId, true);
      // if asked, save status in localStorage
      if (options.clickedId) uify.button.buttonsStatuses[options.clickedId] = true;
      if (options.clickedLocalStorageId) $$.storage.local(options.clickedLocalStorageId, true);
    }
    else {
      // click
      if (options.clickUnselect) options.clickUnselect.call(options.context, ev);
      // set clicked display
      button.setDisplay.unclicked();
      button.clicked = false;
      // if asked, save status
      if (options.clickedId) uify.button.buttonsStatuses[options.clickedId] = false;
      if (options.clickedLocalStorageId) $$.storage.local(options.clickedLocalStorageId, false);
    };
  }

  // CLICK MENU
  else if (options.clickMenu) {
    if (!uify.menu) $$.log.detailedError("uify.button", "To use the clickMenu option, you need to require uify/core/menu in your page.")
    else return function () {
      var menuOptions = $$.defaults(
        {
          title: options.title,
          context: options.context,
        },
        $$.result(options.clickMenu)
      );
      uify.menu(menuOptions);
    };
  }

  // SIMPLE CLICK
  else if (options.click) return _.bind(options.click, options.context);

  // NO (VALID) CLICK FUNCTION
  return function () {};

};

var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/dom");
var uify = require("../_");
var setSelectSate = require("./selectState");
var makeClickFunction = require("./click");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (button) {
  var options = button.options;

  //
  //                              NAME AND CLASSES

  button.name = options.name;

  //
  //                              DISPLAYING BUTTON CONDITION

  if (!_.isUndefined(options.condition) && !$$.result.call(options.context, options.condition)) {
    button.$button.hide();
    return button;
  }
  else button.$button.show();

  //
  //                              IS CLICKED

  button.clicked = undefined;
  if (options.clickedId) button.clicked = uify.button.buttonsStatuses[options.clickedId];
  if (_.isUndefined(button.clicked) && options.clickedLocalStorageId) button.clicked = $$.storage.local(options.clickedLocalStorageId);
  if (_.isUndefined(button.clicked)) button.clicked = $$.result(options.clicked);

  //
  //                              CLASS

  // remove all classes
  button.$button.removeClass();
  button.$button.addClass("uify-button");

  // add custom classes
  if (options.class) button.$button.addClass(options.class);
  if (options.invertColor) button.$button.addClass("invert-colors");

  //
  //                              CREATE INLINE TITLE

  if (options.inlineTitle) {
    button.$button.addClass("with_inline_title with_"+ options.inlineTitle +"_inline_title");
    button.$inlineTitle = button.$button.div({ class: "inline_title", });
  };

  //
  //                              SET ICON

  // icon from icomoon or from image file path
  button.$icon = "no icon defined";
  if (options.icomoon) button.$icon = button.$button.div({
    class: "button_icon icon-"+ (button.clicked && options.icomoonClicked ? options.icomoonClicked : options.icomoon),
  })
  else if (options.icon) {
    button.$icon = button.$button.div({ class: "image_icon", });
    button.$icon.css("background-image", 'url("'+ options.icon +'")');
  };

  //
  //                              TEXT

  if (options.html) button.$button.htmlSanitized(options.html);

  //
  //                              CSS

  if (options.css) button.$button.css(options.css);
  if (options.cssIcon) button.$icon.css(options.cssIcon);

  // custom hover css
  if (options.cssHover) button.$button.hover(function(){
    button.$button.css(options.cssHover);
  }, function(){
    if (button.clicked) button.$button.css(options.cssClicked)
    else button.$button.css(options.css);
  });

  //
  //                              SET CLICKED STATE

  button.setDisplay = setSelectSate(button, options);
  if (button.clicked) button.setDisplay.clicked()
  else button.setDisplay.unclicked();

  //
  //                              CLICK

  // figure out appropriate funciton to execute on click
  button.finalClickFunction = makeClickFunction(button, options);
  // unbind previously set click event on button (in case of refreshing)
  button.$button.unbind("click");
  // apply function to click event
  button.$button.click(button.finalClickFunction);
  // apply keyboard shorcut (if there is one)
  button.bind();

  //
  //                              SETUP TITLE VALUE (now that shortcut has been initialized)

  button.refreshTitle();

  //
  //                              ATTACH BUTTON OPTIONS TO $button DATA

  button.$button.data({ uifyButton: button });

  //
  //                              AFTER AND RETURN

  if (options.after) options.after.call(options.context, button);

  //
  //                              RETURN PROPER BUTTON OBJECT

  return button;

  //                              ¬
  //

};

var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function removeIcon ($icon, options) {
  if (options.icomoonClicked) $icon.removeClass("icon-"+ options.icomoonClicked);
  if (options.icomoon) $icon.removeClass("icon-"+ options.icomoon);
  if (options.icon || options.iconClicked) $icon.css("background-image", "none");
}

function setClickedIcon ($icon, options) {
  if (options.icomoonClicked) $icon.addClass("icon-"+ options.icomoonClicked)
  else if (options.iconClicked) $icon.css("background-image", 'url("'+ options.iconClicked +'")')
  else setUnclickedIcon($icon, options);
}

function setUnclickedIcon ($icon, options) {
  if (options.icomoon) $icon.addClass("icon-"+ options.icomoon)
  else if (options.icon) $icon.css("background-image", 'url("'+ options.icon +'")');
}

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (button, options) {

  return {

    // set clicked display
    clicked: function () {

      // remove current icon and set clicked icon
      removeIcon(button.$icon, options);
      setClickedIcon(button.$icon, options);

      // toggle class
      if (options.clickedClass) button.$button.addClass("clicked");
      // icon css changing
      if (options.cssClicked) button.$button.css(options.cssClicked);

      // set clicked
      button.clicked = true;

    },

    // set unclicked display
    unclicked: function () {

      // remove current icon and set unclicked icon
      removeIcon(button.$icon, options);
      setUnclickedIcon(button.$icon, options);

      // toggle class
      if (options.clickedClass) button.$button.removeClass("clicked");
      // icon css changing
      if (options.css) button.$button.css(options.css);

      // set clicked
      button.clicked = false;

    },

  };

}

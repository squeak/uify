var _ = require("underscore");
var $$ = require("squeak");
var keyboardify = require("keyboardify");
var uify = require("../_");
var makeButton = require("./make");
var makeOptions = require("./options");
var presetTypes = {
  space: require("./presets/space"),
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display a clickable button with nice icon from icomoon or url and many more options
  ARGUMENTS: (
    options <{
      !|$container: <yquerjObject|dom element| should also work with jqueryObject|jqueryString> « you can also pass $container as secondary argument when calling the function, see below »
      ?type: <"space"> « if defined, will create this type of preset button »,
      ?class: <string> «
        some custom class/classes for this button,
        "success", "warning", "error" are preset classes that will automatically color the button accordingly
      »,
      ?name: <string> « name of the button (to make it easier to find it) »,
      ?key: <string> « shortcut combo to use »,
      ?keyid: <string> « if set, this will store the keyboard shortcut set in keyboardify.list, allowing shortcuts to be listed and modified later on »,
      ?title:  <string>,
      ?context: <any>@default=options « any variable passed here will become the context (this) of functions (works with condition, after, click, clickSelect, clickUnselect, clickMenu.click, clickMenu.buttons[].click) »,
      ?condition: <boolean|function(ø){@this=options.context}:<boolean>> « will not display this button if condition is set and is false or returns false »,
      ?prepend: <boolean>@default=false « if true, will prepend this element to the container instead of appending it »,
      ?html: <string> « some custom html to add in the $button »,
      ?click: <function(ev){@this=options.context}> « the action to run when the button is clicked »,
      ?clickMenu: <uify.menu·options|function(ø):<uify.menu·options>> «
        open a menu dialog when clicking on this button
        if this option is defined, the click option is ignored
      »,
      ?after: <function(button){@this=options.context}> « if passed, ran after button creation »,
      // BUTTON STYLING
      ?inlineTitle: <"left"|"right"|"top"|"bottom">@default="undefined",
      ?icomoon: <string>,
      ?icon: <string> « if icomoon is not defined, you can pass a url string to get an image as icon »,
      ?invertColor: <boolean>@default=false « if true, will use primary color for background and contrast for foreground »,
      ?css:  <object>,
      ?cssIcon:  <object> « css applied to the icon only »,
      // MAKING THE BUTTON SELECTABLE
      // the following options turn the button into a selectable button, which means it has two states clicked or not clicked
      // if the clickSelect or clickUnselect options are set, the click option is ignored
      ?clickSelect: <function(ø){@this=options.context}> « the action to run when button is selected »,
      ?clickUnselect: <function(ø){@this=options.context}> « the action to run when button is unselected »,
      ?icomoonClicked: <string>,
      ?iconClicked: <string> « if icomoonClicked is not defined, you can pass a url string to get an image as icon »,
      ?cssClicked:  <object>,
      ?clickedClass: <boolean:default=true> « if true, will set class "clicked" to button when in clicked state»,
      ?clicked: <boolean|function(ø):<boolean>> « set clicked state »,
      ?clickedId: <string> «
        same as clickedLocalStorageId, just it doesn't save status in localStorage, but in the page object, so status is saved until page refreshing
        this option is useful on uify.menu dialog buttons, when the button state must be remembered
        this option has priority over the clickedLocalStorageId localStorage value
      »,
      ?clickedLocalStorageId: <string> «
        if you want uify to remember the click status of your button and restore it from localStorage, set here a unique identifier where to save it to in localStorage
        if you need to fetch the status of the button, you then can use $$.storage.local(clickedLocalStorageId)
        if this option is set, the value in localStorage will overwrite any "clicked" option you may have set
      »,
      // detecting that a button is in a clicked state in no way will trigger it's clickSelect function, the clicked status is used only to notify the button of the state in which the app is an therefore put the button in the appropriate state
    }>,
    !|$container <same as options.$container> « you can pass the container here, if you don't want to pass it in options »,
  )
  RETURN: <{
    name: <string|undefined>,
    $button: <yquerjObject>,
    $icon: <yquerjObject>,
    options: options « see argument »,
    setDisplay: {
      clicked: <function(ø)>,
      unclicked: <function(ø)>,
    },
    click: <function(ø)>,
    unclick: <function(ø)>,
    bind: <function(ø)> « (re)bind the chosen keyboard shortcut from this button (this means the shortcut will aggain trigger the clicking of this button) »,
    unbind: <function(ø)> « unbind the chosen keyboard shortcut from this button (this means the shortcut will not trigger the clicking of this button anymore) »,
    menu: <uify.menu·return> « if clickMenu option is enabled, the button will have this defined »,
    refresh: <function(
      ?newOptions <button·options> « if defined, will replace previous options for this input »
    ):<void>> « refresh button, with the same options or new one (useful for example if you want to reevaluate if the button should be displayed) »,
  }>
*/
uify.button = function (opts, $container) {

  //
  //                              GET ALL OPTIONS AND CHECK IF SHOULD BE DISPLAYED OR NOT

  var options = makeOptions(opts, $container);

  //
  //                              CUSTOM PRESET TYPES

  if (options.type) {
    // make button from preset
    if (presetTypes[options.type]) var options = presetTypes[options.type](options)
    // log can't find this type of button
    else $$.log.detailedError(
      "uify.button",
      "Unknown button preset type: "+ options.type,
      options
    );
  }

  //
  //                              MAKE RETURN OBJECT

  var button = {
    $button: options.$container.div(undefined, options.prepend ? "prepend" : "append"),
    options: options,
    click: function () {
      if (!button.clicked) button.$button.click();
    },
    unclick: function () {
      if (button.clicked) button.$button.click();
    },
    bind: function (bindKey) {
      if (!bindKey) bindKey = button.options.key;
      if (bindKey) {
        var saveToList = button.options.keyid ? {
          keycomboId: button.options.keyid,
          description: button.options.title,
          $uifyRelatedButton: button.$button,
          uifyRefreshButton: function (keycomboObject) { button.refreshTitle(); },
        } : undefined;
        keyboardify.bind(button.options.key, button.finalClickFunction, undefined, undefined, saveToList);
      };
    },
    unbind: function () {
      if (button.options.key) keyboardify.unbind(button.options.key, button.finalClickFunction, undefined, button.options.keyid);
    },
    refresh: function (newOptions) {
      button.$button.empty();
      if (newOptions) {
        options = makeOptions($$.defaults(button.options, newOptions), $container);
        button.options = options;
      };
      makeButton(button);
    },
    refreshTitle: function () {
      var options = button.options;

      // if there is a keyboard shortcut, ask keyboardify title values to use (because it could have been manually modified), else create them
      if (options.key) var titleTexts = keyboardify.getDescriptionTexts(options.keyid || { keycombo: options.key, description: options.title, })
      else var titleTexts = {
        inlineTitle: options.title,
        bubbleTitleAndShortcut: options.title,
      };

      // set title value inline or in a title bubble (with shortcuts appropriately shown)
      if (options.inlineTitle) {
        button.$inlineTitle.htmlSanitized(titleTexts.inlineTitle);
        if (options.key) button.$button.attr("title", "("+ titleTexts.bubbleShortcut +")");
      }
      else button.$button.attr("title", titleTexts.bubbleTitleAndShortcut);

    },
  };

  //
  //                              MAKE THE BUTTON AND RETURN IT (OR null IF CONDITION FAILED)

  return makeButton(button);

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  BUTTONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create multiple buttons at once
  ARGUMENTS: (
    !$container <yquerjObject>,
    !buttonsList <buttonOptions[]>,
  )
  RETURN: <button[]>
*/
uify.buttons = function ($container, buttonsList) {
  return _.map(buttonsList, function (buttonOptions) {
    return uify.button(buttonOptions, $container);
  });
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CLICKED BUTTONS STATUSES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

uify.button.buttonsStatuses = {};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = uify.button;

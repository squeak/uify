
var uify = require("./_");

require("./alert");
require("./button");
require("./confirm");
require("./dialog");
require("./fab");
require("./item");
require("./list");
require("./menu");
require("./panel");
require("./progress");
require("./spaces");
require("./spinner");
require("./tabs");
require("./toast");

module.exports = uify;

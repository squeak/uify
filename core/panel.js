var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");
var uify = require("./_");
require("./item");
require("./tabs");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create a panel in the asked container
  ARGUMENTS: ({
    !$container: <yquerjProcessable>
    ?id: <string> « id to assign to the panel »,
    ?title: <string|function(panel.$el)>,
    ?tabs: <{
      name: <string>,
      content: <string|function($tabContentContainer)>,
    }[]>,
    ?closePanelButton: <boolean>@default=false « if true, will display a cross button usable to close this panel »,
    ?buttons: <uify.button·options[]> « any additional buttons to have in the titlebar »,
  })
  RETURN: <{
    $el: <yquerjObject>,
    titlebar: <uify.item·return>,
    show: <function(ø)> « show panel »,
    hide: <function(ø)> « hide panel »,
    destroy: <function(ø)> « destroy panel »,
    tabs: <uify.tabs·return>,
  }>
*/
uify.panel = function (options) {

  //
  //                              GET CONTAINER AND MAKE SURE IT'S A YQUERJ OBJECT

  var $container = $(options.$container);
  if (!$container.length) $$.log.detailedError("uify.panel", "Panel \"$container\" is undefined or not processable by yquerj. Panel creation will fail.", options.$container);

  //
  //                              CREATE PANEL

  var panel = {
    $el: $container.div({ class: "uify-panel", }),
    show: function(){ panel.$el.show(); },
    hide: function(){ panel.$el.hide(); },
    destroy: function(){ panel.$el.remove(); },
  };

  //
  //                              ADD CUSTOM ATTRIBUTES

  if (options.id) panel.$el.attr("id", options.id);

  //
  //                              ADD TITLE

  if (options.title || options.closePanelButton || options.buttons) panel.titlebar = uify.item({
    $container: panel.$el,
    title: options.title || "",
    class: "uify-panel-title",
    buttons: options.buttons,
  });

  //
  //                              BODY

  panel.$body = panel.$el.div({ class: "uify-panel-body", });

  //
  //                              OPTIONALLY DIRECTLY TABIFY PANEL AND DISPLAY CONTENT

  if (options.tabs) panel.tabs = uify.tabs({
    $container: panel.$body,
    tabs: options.tabs,
  });

  //
  //                              RETURN PANEL

  return panel;

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = uify.panel;

var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/array");
var $ = require("yquerj");
var keyboardify = require("keyboardify");
var uify = require("./_");
require("./button");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DEFAULT BUTTONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function defaultButtons (options) {
  return {

    //
    //                              OK

    ok: {
      id: "ok",
      title: options.okText || "OK",
      key: options.disableEnterKeyValidation ? undefined : "enter",
      click: function (e) {
        if (this.options.ok) return this.options.ok(e);
      },
    },

    //
    //                              CANCEL

    cancel: {
      id: "cancel",
      title: options.cancelText || "cancel",
      key: options.disableEscKeyCancel ? undefined : "esc",
      click: function (e) {
        if (this.options.cancel) return this.options.cancel(e);
      },
    },

    //
    //                              ESC CROSS

    esc: {
      id: "esc",
      icomoon: "cross",
      key: options.disableEscKeyCancel ? undefined : "esc",
      click: function (e) {
        if (this.options.cancel) return this.options.cancel(e);
      },
    },

    //                              ¬
    //

  };
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: create a dialog with simple or advanced contents
  ARGUMENTS: ({

    //
    //                              TITLE AND CONTENT

    ?title: <string|function(ø):<string>> « title of the dialog (in top menubar) »,
    ?subtitle: <string|function(ø):<string>> « subtitle of the dialog (in top menubar) »,
    ?content: <
      | string
      | function($contentContainer<yquerjObject>, options<uify.confirm·options>){@this=dialog<dialog·return>}
    >,

    //
    //                              CUSTOMIZATION

    ?overlay: <boolean>@default=true « if true, will display a dark overlay behind the dialog »,
    ?overlayClickExits: <boolean>@default=true « if true, clicking the overlay will hide the dialog »,
    ?disableEnterKeyValidation: <boolean>@default=false « set this to true to disable enter key from hitting ok button »,
    ?disableEscKeyCancel: <boolean>@default=false « set this to true to disable esc key from hitting cancel/cross button »,
    ?notClosable: <boolean>@default=false «
      setting this to true, will prevent user from exiting dialog appart from clicking ok button
      more precisely: setting this to true is a shorthand for:
        - overlayClickExits: false
        - menuBar.buttons: []
        - buttons: [ allDefaultButtons.ok ]
      if on top of setting this to true, you customize overlayClickExits, menuBar.buttons or buttons, what you set will still overwrite those defaults
    »,
    ?draggable: <boolean>@default=false « if true, the dialog will be movable dragging it with the mouse »,
    ?resizable: <boolean>@default=false « if true, the dialog will be resizable »,

    ?transition: <"fade"|"none">@default="fade" « what transition to use to display the dialog »,
    ?$container: <yquerjProcessable>@default=$("body") « where to display this dialog »,

    //
    //                              STYLE

    ?dialogClass: <string> « class(es) to assign to the whole dialog »,
    ?contentClass: <string> « class(es) to assign to $contentContainer »,
    ?height: <number> « dialog's height: absolute number between 0 and 100 »,
    ?width: <number> « dialog's width: absolute number between 0 and 100 »,

    ?cssDialog: <object> « css to apply to dialog »,
    ?cssContent: <object> « css to apply to dialog content »,
    ?type: <"success"|"error"|"warning"|"danger"> « this will prestyle the dialog »,

    ?okText: <string>@default="OK" « text to display on ok button »,
    ?cancelText: <string>@default="cancel" « text to display on cancel button »,

    //
    //                              CALLBACKS

    ?ok: <function(ø)> « executed if clicked ok, if this returns false the dialog won't be closed »,
    ?cancel: <function(ø)> « executed if clicked cancel or esc key, if this returns false the dialog won't be closed »,

    //
    //                              BUTTONS

    ?buttons: <
      | <"ok"|"cancel"|uify.button·options>[]
      | function(
          <[ allDefaultButtons.cancel, allDefaultButtons.ok ]>,
          allDefaultButtons <allDefaultButtons>,
        ):uify.button·options[]
    >@default=[ allDefaultButtons.cancel, allDefaultButtons.ok ] «
      if the click function of a button returns false, the modal won't be closed when button clicked, otherwise it will
    »
    ?context: <any> «
      if defined, this will be the context (this) of all buttons click events (also buttons in menuBar)
      NOTE: default buttons or any button with id equal to one of the default buttons, will not have this custom context applied to it, because they need to have dialog as context
    »,

    //
    //                              MENU BAR

    ?menuBar: <uify.item>@default={
      title: options.title,
      ?buttons: <
        | uify.button·options[]
        | function(
            <[ allDefaultButtons.esc ]>,
            allDefaultButtons <allDefaultButtons>,
          ):uify.button·options[]
      >@default=[ allDefaultButtons.esc ] «
        if the click function of a button returns false, the modal won't be closed when button clicked, otherwise it will
      »
    } «
      title and comment will be prefilled from options.title and options.subtitle
    »,

    //                              ¬
    //

  })
  RETURN: <{

    options: <uify.dialog·options> « the options you passed »,

    // DOM PARTS
    $overlay: <yquerjObject> « the dark overlay behing the dialog »,
    $dialog: <yquerjObject> « the dialog itself »,
    $header: <yquerjObject> « menu bar »,
    $body: <yquerjObject> « body containing content »,
    $content: <yquerjObject> « where your contens will be appended »,
    $footer: <yquerjObject> « buttons bar »,

    // ELEMENTS
    menuBar: <uify.item·return> « the menu bar item »,
    buttons: <uify.button·return[]> « the list of buttons in footer »,

    // METHODS
    showButton: <function(buttonName<string>) « show the designated button »,
    hideButton: <function(buttonName<string>) « hide the designated button »,
    close: <function(ø)> « close the modal executing cancel function »,
    destroy: <function(ø)> « destroy the modal and all it's contents »,

  }>
  TYPES:
    allDefaultButtons = <{
      ok: {
        id: "ok",
        title: options.okText || "OK",
        key: "enter",
        click: function(e),
      },
      cancel: {
        id: "cancel",
        title: options.cancelText || "cancel",
        key: "esc",
        click: function(e),
      },
      esc: {
        id: "esc",
        icomoon: "cross",
        key: "esc",
        click: function(e),
      },
    }>
*/
uify.dialog = $$.scopeFunction({

  main: function (settings) {

    //
    //                              DEFAULT SETTINGS

    if (!settings) return $$.log.detailedError("uify.dialog", "You didn't specify any settings for uify.dialog.", settings);
    var allDefaultButtons = defaultButtons(settings);
    var defaultSettings = {
      $container: $(settings.$container || "body"),
      transition: "fade",
      overlay: true,
      overlayClickExits: true,
      buttons: [ allDefaultButtons.cancel, allDefaultButtons.ok, ],
      menuBar: {
        _recursiveOption: true,
        title: $$.result(settings.title) || "",
        comment: $$.result(settings.subtitle) || "",
        buttons: [ allDefaultButtons.esc ],
      },
    };
    // NOT CLOSABLE
    if (defaultSettings.notClosable || settings.notClosable) {
      defaultSettings.overlayClickExits = false;
      defaultSettings.menuBar.buttons = [];
      defaultSettings.buttons = [ allDefaultButtons.ok, ];
    };
    settings = $$.defaults(defaultSettings, settings);

    // CREATE A DEDICATED KEYBOARDIFY CONTEXT FOR THE DIALOG
    var newKeyboardContext = "uify-dialog_"+ $$.uuid();
    keyboardify.setContext(newKeyboardContext);

    // BUTTONS SETTINGS
    if (_.isFunction(settings.buttons)) settings.buttons = settings.buttons(defaultSettings.buttons, allDefaultButtons);
    // make it possible to pass just a button name
    settings.buttons = _.compact(_.map(settings.buttons, function (buttonConfig) {
      if (_.isString(buttonConfig)) return allDefaultButtons[buttonConfig]
      else return buttonConfig;
    }));
    if (settings.menuBar && _.isFunction(settings.menuBar.buttons)) settings.menuBar.buttons = settings.menuBar.buttons(defaultSettings.menuBar.buttons, allDefaultButtons);

    //
    //                              CREATE DOM ELEMENTS

    // CREATE DIALOG OVERLAY
    var $overlay = settings.$container.div({ class: "uify-dialog-overlay", });
    if (!settings.overlay) $overlay.addClass("hidden-overlay")
    else if (settings.overlayClickExits) $overlay.click(function () { dialog.close(); });
    if (settings.transition == "fade") $overlay.addClass("fade-in-fast");

    // CREATE DIALOG BASE CONTAINER
    var $dialog = $overlay.div({ class: "uify-dialog", });
    if (settings.dialogClass) $dialog.addClass(settings.dialogClass);
    if (settings.cssDialog) $dialog.css(settings.cssDialog);
    if (settings.type) $dialog.addClass(settings.type);
    $dialog.click(function (e) { e.stopPropagation(); }); // otherwise clicking the dialog activates the clicking of the overlay
    if (!_.isUndefined(settings.width)) $dialog.css("width", settings.width +"vw");
    if (!_.isUndefined(settings.height)) $dialog.css("height", settings.height +"vw");

    // CREATE HEADER, BODY AND FOOTER CONTAINERS
    var $header = $dialog.div({ class: "uify-dialog-header", });
    var $body = $dialog.div({ class: "uify-dialog-body", });
    var $footer = $dialog.div({ class: "uify-dialog-footer", });

    //
    //                              CREATE DIALOG OBJECT AND ATTACH METHODS TO IT

    var dialog = {
      $overlay: $overlay,
      $dialog: $dialog,
      $header: $header,
      $body: $body,
      $footer: $footer,
      options: settings,
      showButton: function (buttonName) {
        var butt = _.findWhere(dialog.buttons, { name: buttonName, });
        if (!butt) return $$.log.detailedError("uify.dialog", "Could not show button named: "+ buttonName +" it could not be found.");
        butt.refresh({ condition: true, });
      },
      hideButton: function (buttonName) {
        var butt = _.findWhere(dialog.buttons, { name: buttonName, });
        if (!butt) return $$.log.detailedError("uify.dialog", "Could not hide button named: "+ buttonName +" it could not be found.");
        butt.refresh({ condition: false, });
      },
      close: function () {
        var cancelResult = settings.cancel ? settings.cancel() : true;
        if (cancelResult !== false) dialog.destroy();
      },
      destroy: function () {
        keyboardify.cancelContext(newKeyboardContext);
        dialog.$overlay.remove();
        delete dialog;
      },
      hide: function () {
        keyboardify.cancelContext(newKeyboardContext);
        dialog.$overlay.hide();
      },
      show: function () {
        keyboardify.setContext(newKeyboardContext);
        dialog.$overlay.show();
      },
    };

    //
    //                              CUSTOMIZE BUTTONS TO CLOSE DIALOG WHEN THEY ARE CLICKED AND TO MAKE THEM LOOK NICER

    function setupClickAndDestroy (buttonOpts, key) {
      var oldClick = buttonOpts[key];
      buttonOpts[key] = function (e) {
        if (buttonOpts.context) var context = _.indexOf(_.pluck(allDefaultButtons, "id"), buttonOpts.id) == -1 ? buttonOpts.context : dialog
        else var context = dialog;
        var resultOfClicking = oldClick.call(context, e);
        if (resultOfClicking !== false) dialog.destroy();
      };
    };

    function customizeClickEvent (buttonOpts) {
      if (buttonOpts.click || buttonOpts.clickSelect || buttonOpts.clickUnselect) {
        if (buttonOpts.click) setupClickAndDestroy(buttonOpts, "click");
        if (buttonOpts.clickSelect) setupClickAndDestroy(buttonOpts, "clickSelect");
        if (buttonOpts.clickUnselect) setupClickAndDestroy(buttonOpts, "clickUnselect");
      }
      else buttonOpts.click = function () { dialog.destroy(); };
    };

    _.each(settings.buttons, function (buttonOpts) {
      buttonOpts.invertColor = true;
      buttonOpts.inlineTitle = true;
      customizeClickEvent(buttonOpts);
    });
    if ($$.getValue(settings, "menuBar.buttons")) _.each(settings.menuBar.buttons, function (buttonOpts) { customizeClickEvent(buttonOpts); });

    // IF context IS DEFINED, PASS IT TO ALL BUTTONS OPTIONS ELSE PASS dialog
    var buttonsContext = !_.isUndefined(settings.context) ? settings.context : dialog;
    _.chain([
      settings.buttons,
      $$.getValue(settings, "menuBar.buttons"),
      $$.getValue(settings, "menuBar.avatar")]
    )
      .flatten()
      .compact()
      .each(function (button) {
        if (!button.context) button.context = buttonsContext;
      })
    ;

    //
    //                              CREATE CONTENTS

    // CREATE MENUBAR
    settings.menuBar.$container = $header;
    dialog.menuBar = uify.item(settings.menuBar);

    // CREATE CONTENT
    dialog.$content = $body.div({ class: "uify-dialog-content", });
    if (settings.contentClass) dialog.$content.addClass(settings.contentClass);
    if (settings.cssContent) dialog.$content.css(settings.cssContent);
    if (_.isFunction(settings.content)) settings.content.call(dialog, dialog.$content)
    else dialog.$content.htmlSanitized(settings.content);

    // CREATE FOOTER BUTTONS
    dialog.buttons = uify.buttons($footer, settings.buttons);

    //
    //                              MAKE DIALOG DRAGGABLE OR RESIZABLE IF ASKED

    if (dialog.options.draggable) {
      $$.dom.makeDraggable({ $target: dialog.$dialog, $handle: $header, });
      $$.dom.makeDraggable({ $target: dialog.$dialog, keyToPress: "altKey", });
    };
    if (dialog.options.resizable) dialog.$dialog.css({
      resize: "both",
      overflow: "hidden",
      minHeight: "unset",
      maxHeight: "unset",
      minWidth: "unset",
      maxWidth: "unset",
    });

    //
    //                              RETURN DIALOG OBJECT

    return dialog;

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FULLSCREEN
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display and image or custom content fullscreen, closable by clicking anywhere
    ARGUMENTS: (
      contentOrOptions <uify.dialog.fullscreen·content|{
        content: <uify.dialog.fullscreen·content>,
        hideOnClose: <boolean>@default=false « if true, clicking the cross will not destroy the dialog, but only hide it »,
        preventHidingOnClick: <boolean>@default=false « if true, clicking anywhere will not close the dialog »,
        onDestroy: <function(uify.dialog.fullscreen·return)> « if defined, will run this function before destroying the dialog (not before hiding, so this option is not compatible with hideOnClose option) »,
      }>
    )
    RETURN: <{
      $el: <yquerjObject>,
      hideOrDestroy: <function(ø)>,
      destroy: <function(ø)>,
      hide: <function(ø)>,
      show: <function(ø)>,
      toggle: <function(ø)>,
      closeButton: <uify.fab·return>,
    }>
    TYPES:
      uify.dialog.fullscreen·content = <string|function($container<yquerjObject>)> «
        if string, should be an image url
        if function, will receive the fullscreen container, you can do what you want with it
      »
  */
  fullscreen: function (options) {

    // support passing only content and not options object
    if (!$$.isObjectLiteral(options)) options = { content: options, };

    // create fullscreen container
    var $fullscreenContent = $("body").div({ class: "uify-dialog-fullscreen", });

    // create fullscreen content
    if (_.isString(options.content)) $fullscreenContent.css("background-image", 'url("'+ options.content +'")')
    else if (_.isFunction(options.content)) options.content($fullscreenContent)
    else $$.log.error("uify.dialog.fullscreen: Invalid \"content\", should be either a string or a function.", options.content);

    // create a dedicated keyboardify context for the dialog
    var newKeyboardContext = "uify-dialog-fullscreen_"+ $$.uuid();
    keyboardify.setContext(newKeyboardContext);

    // close on click anywhere
    if (!options.preventHidingOnClick) $fullscreenContent.click(function (e) { uifyDialogFullscreen.hideOrDestroy(); });

    // return
    var uifyDialogFullscreen = {
      $el: $fullscreenContent,
      hideOrDestroy: function () {
        if (options.hideOnClose) uifyDialogFullscreen.hide()
        else uifyDialogFullscreen.destroy();
      },
      destroy: function () {
        if (options.onDestroy) options.onDestroy(uifyDialogFullscreen);
        keyboardify.cancelContext(newKeyboardContext);
        $fullscreenContent.remove();
      },
      hide: function () {
        keyboardify.cancelContext(newKeyboardContext);
        $fullscreenContent.hide();
      },
      show: function () {
        keyboardify.setContext(newKeyboardContext);
        $fullscreenContent.show();
      },
      toggle: function () {
        if ($fullscreenContent.is(":hidden")) keyboardify.setContext(newKeyboardContext)
        else keyboardify.cancelContext(newKeyboardContext);
        $fullscreenContent.toggle();
      },
    };

    // additional close button
    uifyDialogFullscreen.closeButton = uify.fab({
      $container: $fullscreenContent,
      icomoon: "cross",
      click: function (e) { uifyDialogFullscreen.hideOrDestroy(); },
      position: { top: "default", right: "default", },
      size: 25,
      space: 10,
      title: "exit",
      key: "esc",
    });

    return uifyDialogFullscreen;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = uify.dialog;

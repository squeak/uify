var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var positionsCorrespondances = {
  left: ["border-top-left-radius", "border-bottom-left-radius"],
  right: ["border-top-right-radius", "border-bottom-right-radius"],
  top: ["border-top-left-radius", "border-top-right-radius"],
  bottom: ["border-bottom-left-radius", "border-bottom-right-radius"],
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function setPosition (button, positionName, value, dimensionOfOtherFabsInSamePosition) {

  // DIFFERENT POSITION CASES
  if (positionName == "center") {
    positionName = "left";
    var positionValue = (button.options.$container.width()/2) - (button.options.size/2);
  }
  else if (positionName == "middle") {
    positionName = "top";
    var positionValue = (button.options.$container.height()/2) - (button.options.size/2);
  }
  else var positionValue = value;

  // ADD VALUE FOR CURRENT DIRECTION
  if (button.options.direction == "vertical" && (positionName == "top" || positionName == "bottom" || positionName == "middle"))  positionValue += dimensionOfOtherFabsInSamePosition
  else if (button.options.direction == "horizontal" && (positionName == "left" || positionName == "right" || positionName == "center"))  positionValue += dimensionOfOtherFabsInSamePosition;

  // SET POSITION IN CSS
  button.$button.css(positionName, positionValue);

}

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (button, dimensionOfOtherFabsInSamePosition) {

  _.each(button.options.position, function (posDetails, posName) {

    // STICKING FAB
    if (posDetails == "stick") {
      // set position
      setPosition(button, posName, 0, dimensionOfOtherFabsInSamePosition);
      // no rounding
      _.each(positionsCorrespondances[posName], function (posCorr) { button.$button.css(posCorr, 0); });
      // change margin
      button.$icon.css("margin-"+ posName, "-10px");
    }

    // DEFAULT OR CUSTOM DISTANCE
    else if (posDetails == "default") setPosition(button, posName, button.options.space, dimensionOfOtherFabsInSamePosition)
    else setPosition(button, posName, posDetails, dimensionOfOtherFabsInSamePosition);

  });

}

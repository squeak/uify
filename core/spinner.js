var _ = require("underscore");
var $$ = require("squeak");
var uify = require("./_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SPINNER CREATION
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function create (spinner) {

  // create container
  spinner.$el = spinner.options.$container.div({ class: "uify-spinner", });

  // create overlay
  if (spinner.options.overlay) spinner.$overlay = spinner.$el.div({ class: "uify-spinner_overlay", });
  if (_.isString(spinner.options.overlay)) {
    // do not user "background" css property because it overwrites background-size set in stylesheet
    if (spinner.options.overlay.match(/^url\(.*\)$/)) spinner.$overlay.css("background-image", spinner.options.overlay)
    else spinner.$overlay.css("background-color", spinner.options.overlay);
  };

  // create spinner
  spinner.$spinner = spinner.$el.div({ class: "uify-spinner_spinner icon-"+ spinner.options.icon, });
  if (spinner.options.size) spinner.$spinner.css({
    fontSize: spinner.options.size +"vh",
    top: "calc(50% - "+ (spinner.options.size/2) +"vh)",
    left: "calc(50% - "+ (spinner.options.size/2) +"vh)",
  });

  // create text
  if (spinner.options.text) spinner.$text = spinner.$el.div({ class: "uify-spinner_text", htmlSanitized: spinner.options.text, });

  // set created to true
  spinner.created = true;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display a spinner in the given container or main body
  ARGUMENTS: (
    ?options: <{
      ?$container: <yquerjProcessable>,
      ?text: <string> « some text to display under the spinner »,
      ?overlay: <boolean|string>@default=false «
        if true display an overlay behind the spinner
        if it's a string, will be applied as "background-color" or
        if it follows the pattern 'url("/path/to/image.jpg")', as "background-image"
      »,
      ?delay: <number> «
        if this is defined, will not display the spinner right away, but wait this delay to show it,
        like this if the spinner is if the spinner is created and removed in less time than this delay, it will jus not be shown, avoiding an ugly flash
        number in milliseconds
      »,
      ?size: <number>@default=20 «
        size of the spinner in window height percentage (vh)
        the default value is not applied through javascript and is therefore easy to customize with css, any value set here will be applied with jquery.css method
      »,
      ?icon: <string>@default="spinner" « the icomoon icon to use as spinner »,
    }>,
  )
  RETURN: <{
    destroy: <function(ø)>,
    options: <object> « just the options you passed to uify.spinner when you called it »,
    $el: <yquerjObject>,
    $spinner: <yquerjObject>,
    $text: <yquerjObject>,
    $overlay: <yquerjObject>,
    created: <boolean> « true if the spinner has already been created »,
    destroyed: <boolean> « true if the spinner has been destroyed »,
  }>
*/
uify.spinner = function (options) {

  // get all options
  var defaultOptions = {
    $container: options ? $(options.$container || "body") : $("body"),
    icon: "spinner",
  };
  options = $$.defaults(defaultOptions, options);

  // create spinner object
  var spinner = {
    options: options,
    destroy: function () {
      // fade it out and then remove it
      if (spinner.created) {
        spinner.$el.css("opacity", 0);
        setTimeout(function () { spinner.$el.remove(); }, 300);
      }
      spinner.destroyed = true;
    },
  };

  // create spinner (with or without delay)
  if (options.delay) setTimeout(function () { if (!spinner.destroyed) create(spinner) }, options.delay);
  else create(spinner);

  // return
  return spinner;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = uify.spinner;

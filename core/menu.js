var _ = require("underscore");
var $$ = require("squeak");
var uify = require("./_");
require("./button");
require("./dialog");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display a menu dialog (action sheet) to choose between different actions
  ARGUMENTS: ({
    ?title: <string|function(ø):<string>> « menu title »,
    ?subtitle: <string|function(ø):<string>> « menu subtitle »,
    !buttons: <uify.button·options[]>,
    ?click: <function(buttonOptions<uify.button·options>, e)> « it no click function is attached to a button, this method will be used instead »,
    ... any uify.dialog options
  })
  RETURN: <uify.dialog·return>
*/
uify.menu = function (options) {

  //
  //                              MAKE DIALOG

  // attach global click action if there is one and button doesn't have click action
  if (options.click) _.each(options.buttons, function (buttonOptions) {
    if (!buttonOptions.click) buttonOptions.click = function (e) { return options.click.call(this, buttonOptions, e) };
  });

  // create dialog
  var dialog = uify.dialog(options);
  dialog.$dialog.addClass("uify-menu");

  // if no title, hide dialog header
  if (!options.title) dialog.$header.hide();

  return dialog;

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = uify.menu;

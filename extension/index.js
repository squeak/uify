'use strict';

var uify = require("../core");

require("./calendar");
require("./differences");
require("./preview");
require("./swiper");
require("./wiki");

module.exports = uify;

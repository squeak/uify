var _ = require("underscore");
var $$ = require("squeak");
var swipablePreview = require("./swipablePreview")
var uify = require("../../core");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

var previewify = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ICON PREVIEW
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION:
      display an icon adapted to the file(s) to preview, and when clicked opening a fullscreen preview dialog
      if a single file is passed, and it's an image, will display a thumbnail of this image
    ARGUMENTS: (
      !$container <yquerjObject> « where to display the icon »,
      !uifyPreview <uify.preview·return>,
      ?callback <function(uifyPreview)> « executed when finished »,
    )
    RETURN: <void>
  */
  icon: function ($container, uifyPreview, callback) {

    // no contents, just do nothing
    if (!uifyPreview.contents.length) return;

    // figure out icon to use for contents
    if (uifyPreview.contents.length > 1) var iconToUse = "files-empty"
    else {
      var ct = uifyPreview.contents[0].contentType;
      if (ct === "image") var iconToUse = null
      else if (ct === "text") var iconToUse = "file-text2"
      else if (ct === "video") var iconToUse = "file-video"
      else if (ct === "audio") var iconToUse = "file-audio-o"
      else if (ct === "pdf") var iconToUse = "file-pdf"
      else if (ct === "iframe") var iconToUse = "earth"
      else var iconToUse = "attachment2";
    };

    // display preview thumbnail
    // icon
    if (iconToUse) {
      uifyPreview.$icon = $container.div({
        class: "uify-preview-icon icon-"+ iconToUse,
      }).click(function () {
        previewify.dialog(uifyPreview);
      })
    }
    // image
    else uifyPreview.contents[0].data(function (imageUrl) {
      uifyPreview.$icon = $container.img({
        class: "uify-preview-icon",
        src: imageUrl,
      }).click(function () {
        previewify.dialog(uifyPreview);
      });
    });

    // run eventual callback
    if (_.isFunction(callback)) callback(uifyPreview);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SWIPABLE REDUCED PREVIEW
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display a small swipable preview of the files, when clicked it opens a dialog with the content fullscreen
    ARGUMENTS: (
      !$container <yquerjObject> « where to display the reduced previews »,
      !uifyPreview <uify.preview·return>,
      ?callback <function(uifyPreview)> « executed when finished »,
    )
    RETURN: <void>
  */
  reduced: function ($container, uifyPreview, callback) {

    $container.addClass("uify-preview-reduced");

    // display swipable preview
    swipablePreview({
      $container: $container,
      uifyPreview: uifyPreview,
      small: true,
      swiperOptions: { spaceBetween: 15, },
      callback: function (uifySwiper) {

        // save uifySwiper object to uifyPreview
        uifyPreview.reduced = uifySwiper;

        // callback
        if (_.isFunction(callback)) callback(uifyPreview);

        // maximize on double click
        uifySwiper.$dom.slidesContainer.dblclick(function () {
          previewify.dialog(uifyPreview, undefined, uifyPreview.reduced.swiper.activeIndex);
        });

      },
    });

    // button to maximize
    uify.button({
      $container: $container,
      icomoon: "expand",
      class: "uify-preview-reduced-maximize",
      click: function () {
        previewify.dialog(uifyPreview, undefined, uifyPreview.reduced.swiper.activeIndex);
      },
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DIALOG WITH SWIPABLE CONTENT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display a fullscreen dialog previewing the specified files
    ARGUMENTS: (
      !uifyPreview <uify.preview·return>,
      ?callback <function(uifyPreview)> « executed when finished »,
      ?startingIndex <integer> « at which slide to open the swiper »,
    )
    RETURN: <void>
  */
  dialog: function (uifyPreview, callback, startingIndex) {

    // display dialog
    uifyPreview.dialog = uify.dialog.fullscreen(function ($container) {
      // add class to fullscreen dialog container
      $container.addClass("uify-preview-dialog");
      // generate swipable preview in fullscreen dialog
      swipablePreview({
        $container: $container,
        uifyPreview: uifyPreview,
        swiperOptions: { initialSlide: startingIndex, },
        callback: function (uifySwiper) {
          // make sure that clicking on arrows and other swiper interractions don't close the dialog
          uifySwiper.$dom.container.click(function (e) { e.stopPropagation(); });
          // run eventual callback
          if (_.isFunction(callback)) callback(uifyPreview);
        },
      });
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = previewify;

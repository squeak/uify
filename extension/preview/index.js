var _ = require("underscore");
var $$ = require("squeak");
var uify = require("../../core");
var previewify = require("./previewify")

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  INTERPRET CONTENT TYPE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function interpretContentType (contentType) {
  if (!contentType) return "unknown"
  else if (contentType.match(/^image\//)) return "image"
  else if (contentType.match(/^video\//)) return "video"
  else if (contentType.match(/^audio\//)) return "audio"
  else if (contentType.match(/^text\/markdown/)) return "markdown"
  else if (contentType.match(/^text\//) || contentType.match(/^application\/x\-/)) return "text";
  else if (contentType.match(/^application\/pdf/)) return "pdf";
  else if (contentType === "iframe") return "iframe";
  else return "unknown";
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GET BLOB CONTENT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var dataGetter = {

  // GET DATA FROM BLOB
  blob: function (blob, contentType, callback) {
    if (contentType === "video" || contentType === "audio" || contentType === "pdf") callback(window.URL.createObjectURL(blob))
    else {
      var reader = new FileReader();
      reader.onload = function (e) { callback(e.target.result); };
      reader.readAsDataURL(blob);
    };
  },

  // GET DATA FROM URL
  url: function (url, callback) {
    callback(url);
  },

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: display in a dialog, a text, pdf, video, audio, image, list of images
  ARGUMENTS: ({
    !content: <blob|blob[]|file|file[]|uifyPreviewContentOption|uifyPreviewContentOption[]|string|string[]> «
      the url, blob, file, or list of any of those elements to display
      if you pass directly a url, it will assume the content is an image, pass an object specifying the content type if it's something else
    »,
    ?$clickableThumbnailContainer: <yquerjObject>@default=undefined « if defined, will not display the preview, but create in this container a small dom element preview that clicked will open the bigger preview dialog »,
    ?displayOnlyIconThumbnail: <boolean>@default=false «
      this option is only useful if you defined $clickableThumbnailContainer
      if true, will not display swipable thumbnails, but just a small text and icon
    »,
    ?callback: <function(uify.preview·return)> « executed when content has finished loading »,
  })
  RETURN: <{
    options <uify.preview·options> « options passed when creating the preview »,
    contents: <uifyPreviewContent[]> « the contents displayed in the preview »,
    ?dialog: <uify.dialog.fullscreen·return|undefined> « the dialog containing preview, if undefined means that it's closed »,
    ?reduced: <uify.swiper·return> « if clickable thumbnail was asked, this will be the corresponding uify.swiper »,
    ?$icon: <yquerjObject> « if clickable icon thumbnail was asked, this will be the corresponding icon element »,
  }>
  TYPES:
    uifyPreviewContentOption = <{
      !content: <string|blob|file> « the url or blob or file to preview »,
      !contentType: <string> «
        any supported content type string (see NOTES below for an exhaustive list of what's supported)
        if content is a blob or file, will try to figure it out automatically
        if content is a url, and this is undefined, will assume it's an image
        if content is a url to a page to open (whether it contains text or html content to preview, pass "iframe" as contentType)
      »,
      ?title: <string> « an html string, title for this content »,
    }>
    uifyPreviewContent = <{
      content: <string> « string, url, blob, file »,
      contentType: <"image"|"vide"|"audio"|"pdf"|"text"|"iframe"|"unknown">,
      data: <function(
        callback <function(dataString <string>)>,
      )> « an asynchronous function that gets the actual data to display for the given content »,
      ?title: <string> « an html string, title for this content »,
    }>
  NOTES:
    The following type of contents are currently supported (any other content type will result in the preview just proposing to download the content):
      image/*          < interpreted as image
      text/markdown    < interpreted as markdown text
      text/*           < interpreted as text
      application/x-*  < interpreted as text
      application/pdf  < interpreted as pdf
      video/*          < interpreted as video
      audio/*          < interpreted as audio
      iframe           < display the passed url creating an iframe
*/
uify.preview = function (options) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DEFAULT OPTIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  var defaultOptions = {
    displayOnlyIconThumbnail: false,
  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CREATE PREVIEW OBJECT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  var uifyPreview = {
    options: $$.defaults(defaultOptions, options),
  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FIGURE OUT CONTENT TYPE AND DATA, AND KICK IN
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // MAKE SURE CONTENTS IS AN ARRAY
  uifyPreview.contents = _.isArray(uifyPreview.options.content) ? uifyPreview.options.content : [uifyPreview.options.content];

  // BUILD THE CONTENTS OBJECT WITH APPROPRIATE DATA MAKING FUNCTIONS
  _.each(uifyPreview.contents, function (content, contentIndex) {

    // blob was passed
    if ($$.isBlob(content)) uifyPreview.contents[contentIndex] = {
      content: content,
      contentType: interpretContentType(content.type),
      data: _.partial(dataGetter.blob, content, interpretContentType(content.type)),
    }

    // url was passed
    else if (_.isString(content)) uifyPreview.contents[contentIndex] = {
      content: content,
      contentType: "image",
      data: _.partial(dataGetter.url, content),
    }

    // object was passed
    else {
      var ct = interpretContentType(content.contentType || ($$.isBlob(content.content) ? content.contentType : undefined));
      uifyPreview.contents[contentIndex] = {
        content: content.content,
        contentType: ct,
        title: content.title,
        data: $$.isBlob(content.content) ? _.partial(dataGetter.blob, content.content, ct) : _.partial(dataGetter.url, content.content),
      };
    };

  });

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CREATE THUMBNAIL OR DIALOG
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  if (uifyPreview.options.$clickableThumbnailContainer) {
    if (uifyPreview.options.displayOnlyIconThumbnail) previewify.icon(uifyPreview.options.$clickableThumbnailContainer, uifyPreview, uifyPreview.options.callback)
    else previewify.reduced(uifyPreview.options.$clickableThumbnailContainer, uifyPreview, uifyPreview.options.callback);
  }
  else previewify.dialog(uifyPreview, uifyPreview.options.callback);

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  RETURN
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  return uifyPreview;

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = uify.preview;

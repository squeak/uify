var _ = require("underscore");
var $$ = require("squeak");
var uify = require("../../core");
require("../swiper");
var markdownify = require("markdownify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  PREVIEWER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var previewer = {

  //
  //                              IMAGE

  image: function ($container, contentData, small) {
    $container.css("background-image", 'url("'+ contentData +'")');
  },

  //
  //                              PDF

  pdf: function ($container, contentData, small) {
    // small icon to open pdf bigger
    if (small) return $container.div({ class: "icon-file-pdf", })
    // pdf full display
    else return $container.embed({ src: contentData, }).click(function (e) { e.stopPropagation(); });
  },

  //
  //                              AUDIO

  audio: function ($container, contentData, small) {
    // if small, an icon that toggles playing
    if (small) {
      var $content = $container.div({ class: "icon-play2", }).click(function () {
        $content.toggleClass("icon-play2 icon-pause");
        $audio[0].paused ? $audio[0].play() : $audio[0].pause();
      });
      var $audio = $container.audio({ src: contentData, }).css("display", "none");
      $audio[0].oncanplaythrough = function () {
        $content.div({ text: Math.round($audio[0].duration) +"s", });
      };
      return $content;
    }
    // else a full audio player
    else return $container.audio({
      src: contentData,
      controls: true,
    }).click(function (e) { e.stopPropagation(); });
  },

  //
  //                              VIDEO

  video: function ($container, contentData, small) {
    return $container.video({
      src: contentData,
      controls: !small,
    }).click(function (e) {
      if (small) this.paused ? this.play() : this.pause();
      e.stopPropagation();
    });
  },

  //
  //                              MARKDOWN

  markdown: function ($container, contentData, small) {
    var textContent = atob(contentData.replace(/^.*(base64,)/, ""));
    textContent = markdownify(textContent);
    return previewer.text($container, undefined, small, textContent);
  },

  //
  //                              TEXT

  text: function ($container, contentData, small, textContent) {

    // figure out text content from contentData
    if (!textContent) textContent = atob(contentData.replace(/^.*(base64,)/, ""));

    return $container.div({
      htmlSanitized: $$.string.htmlify(textContent) +"<br><br>", // NOTE: add line breaks at end because bottom padding is not respected
    }).click(function (e) { e.stopPropagation(); });

  },

  //
  //                              IFRAME

  iframe: function ($container, contentData, small) {
    return $container.iframe({ src: contentData, });
  },

  //
  //                              UNKNOWN

  unknown: function ($container, contentData, small, contentType) {
    return $container.div({
      htmlSanitized: 'The content could not be previewed.<br>(content type: '+ contentType +')<br>You can download this file clicking the <span class="icon-download3"></span> icon below.',
    });
  },

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GENERATE SINGLE PREVIEW
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display preview for the passed content
  ARGUMENTS: ({
    !$container <yquerjObject>,
    !contentObject <{
      !contentType: <string>,
      !data: <function(function(contentData))>,
    }>,
    ?small <boolean>@default=false « if true, will reduce complexity of the preview to make it more adapted to be displayed small »,
    ?done <function(ø)> « callback executed when preview has been generated »,
  })
  RETURN: <void>
*/
function generateSinglePreview ($container, contentObject, small, done) {

  // get content data
  contentObject.data(function (contentData) {

    // DISPLAY CONTENT ACCORDING TO DATA TYPE
    if (previewer[contentObject.contentType]) var $content = previewer[contentObject.contentType]($container, contentData, small)
    else var $content = previewer.unknown($container, contentData, small, contentObject.contentType);

    // ADD CONTENT CLASSES TO CONTENT
    if ($content) {
      // general
      $content.addClass("uify-preview-content");
      // small
      if (small) $content.addClass("uify-preview-content_small");
      // content specific class
      if (previewer[contentObject.contentType]) $content.addClass("uify-preview-content_"+ contentObject.contentType)
      else $content.addClass("uify-preview-content_unknown");
    };

    // DISPLAY TITLE (IF NOT SMALL)
    if (!small && contentObject.title) {
      $container.div({
        class: "uify-preview-content-title",
        text: contentObject.title,
      })
    };

    // DOWNLOAD BUTTON
    uify.button({
      $container: $container,
      icomoon: "download3",
      class: "uify-preview-content-download_button",
      click: function () {
        var $temporaryLink = $("body").a({ href: contentData, download: contentObject.title || $$.uuid(), });
        $temporaryLink[0].click();
        $temporaryLink.remove();
      },
    });

    // RUN PREVIEW CREATION CALLBACK
    if (done) done();

  });

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: display swipable previews for the given contents
  ARGUMENTS: ({
    $container: <yquerjObject>,
    uifyPreview: <uify.preview·return>,
    small: <boolean>,
    swiperOptions: <see https://swiperjs.com/swiper-api#parameters>,
    callback: <function(?uifySwiper<uify.swiper·return>)>,
  })
  RETURN: <void>
*/
module.exports = function (options) {
  var contents = options.uifyPreview.contents;

  // add preview class
  options.$container.addClass("uify-preview");

  // display swiper containing previews
  uify.swiper({
    $target: options.$container,
    slides: contents,
    swiperOptions: options.swiperOptions,
    slideMaker: function ($swipeContainer, contentObject, done) {
      return generateSinglePreview($swipeContainer, contentObject, options.small, done);
    },
    async: true,
    asyncCallback: options.callback,
  });

};

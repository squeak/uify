var _ = require("underscore");
var $$ = require("squeak");
require("squeak/plugin/compare");
var uify = require("../core");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: a little interface to display differences between two passed objects (or what was removed and added between two versions of an object)
  ARGUMENTS: ({
    !$container: <yquerj> « where to display the differences »,
    !before: <object> « first object for which to compare differences »,
    !after: <object> « second object witch which to compare differences »,
    ?displayBefore: <boolean|string> « if defined, will display the before object (if string, will use this value as title to introduce it) »,
    ?displayAfter: <boolean|string> « if defined, will display the after object (if string, will use this value as title to introduce it) »,
  })
  RETURN: <{
    options: <uify.differences·options>,
    $el: <yquerjElement>,
    differences: <$$.compare.smarterDifferences·return>,
  }>
*/
uify.differences = $$.scopeFunction({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAIN
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    see uify.differences
  */
  main: function (options) {

    // calculate differences
    var differences = $$.compare.smarterDifferences(options.before, options.after);

    // create container
    var $contentContainer = options.$container.div({ class: "uify-differences", })

    // DISPLAY BEFORE OBJECT IF ASKED
    if (options.displayBefore) {
      var $before = $contentContainer.div({ class: "uify-differences-before", });
      $before.div({
        class: "uify-differences-title",
        text: _.isString(options.displayBefore) ? options.displayBefore : "Before:",
      });
      $before.pre({ text: $$.json.pretty(options.before), });
    };

    // DISPLAY AFTER OBJECT IF ASKED
    if (options.displayAfter) {
      var $after = $contentContainer.div({ class: "uify-differences-after", });
      $after.div({
        class: "uify-differences-title",
        text: _.isString(options.displayAfter) ? options.displayAfter : "After:",
      });
      $after.pre({ text: $$.json.pretty(options.after), });
    };

    // DISPLAY REMOVED PARTS
    var $removed = $contentContainer.div({ class: "uify-differences-removed", });
    $removed.div({
      class: "uify-differences-title",
      text: "Removed content:"
    });
    $removed.pre({ text: $$.json.pretty(differences.obj1), });

    // DISPLAY ADDED PARTS
    var $added = $contentContainer.div({ class: "uify-differences-added", });
    $added.div({
      class: "uify-differences-title",
      text: "Added content:"
    });
    $added.pre({ text: $$.json.pretty(differences.obj2), });

    // TRY TO RETURN SOMETHING USEFUL
    return {
      options: options,
      $el: $contentContainer,
      differences: differences,
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ALERT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: alert differences between two objects in a dialog
    ARGUMENTS: ( option <see uify.differences·options> « without need for $container » )
    RETURN: <void>
  */
  alert: function (options) {
    uify.alert({
      title: options.title,
      width: 80,
      dialogClass: "uify-differences-alert",
      content: function ($contentContainer, opts) {
        var fullOptions = $$.defaults(options, { $container: $contentContainer, });
        uify.differences(fullOptions);
      },
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CONFIRM
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display a confirmation dialog
    ARGUMENTS: ({
      see uify.differences·options (without need for $container)
      !text: <string> « what you want confirm for (will be dispayed as dialog content) »,
      ?ok: <function(ø)> « ran if user clicked ok button »,
      ?cancel: <function(ø)> « ran if user clicked cancel button »,
    })
    RETURN: <void>
  */
  confirm: function (options) {

    var uifyDifferences;
    var $dialogContainer;
    uify.confirm({
      type: "warning",
      dialogClass: "uify-differences-confirm",
      content: function ($container) {

        // message
        $container.div({
          class: "uify-differences-confirm-message",
          text: options.text,
        });

        // useful for "show modifications" button
        $dialogContainer = $container;

      },
      buttons: [
        // o            >            +            #            °            ·            ^            :            |

        {
          title: "show modifications",
          click: function () {

            // created differences display (if first time)
            if (!uifyDifferences) uifyDifferences = uify.differences($$.defaults(options, { $container: $dialogContainer, }))
            // else toggle it's display
            else uifyDifferences.$el.toggle();

            // prevent from closing dialog
            return false;

          },
        },

        {
          title: "cancel",
          click: function () { if (_.isFunction(options.cancel)) options.cancel(); },
        },

        {
          title: "ok",
          click: function () { if (_.isFunction(options.ok)) options.ok(); },
        },

        // o            >            +            #            °            ·            ^            :            |
      ],
    });

  },
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});

var _ = require("underscore");
var $$ = require("squeak");
var $$log = $$.logger("uify.wiki");
var uify = require("../core");

var uifyWikiMethods = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INITIALIZE SIDEBAR
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: create wiki page sidebar
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  initializeSidebar: function () {
    var uifyWiki = this;

    // LOGO AND TITLE
    var $logoAndTitle = uifyWiki.$sidebar.a("logo" ).click(function () {
      $$.url.query.set("");
      uifyWiki.switchPage(uifyWiki.options.abstract);
    });
    if (uifyWiki.options.logoUrl) $logoAndTitle.img({ src: uifyWiki.options.logoUrl, });
    if (uifyWiki.options.title) $logoAndTitle.div({ html: uifyWiki.options.title, });

    // CONTENT
    uifyWiki.$sidebarContent = uifyWiki.$sidebar.div("content");

    // SIDEBAR FOOTER
    if (uifyWiki.options.sidebarFooter) uifyWiki.$sidebar.div({
      class: "footer",
      htmlSanitized: _.isFunction(uifyWiki.options.sidebarFooter) ? _.partial(uifyWiki.options.sidebarFooter, _, uifyWiki) : uifyWiki.options.sidebarFooter,
    });

    // SIDEBAR BUTTON
    uify.button({
      $container: uifyWiki.$container,
      icomoon: "menu",
      class: "uify-wiki-sidebar_button",
      click: function () {
        uifyWiki.$container.toggleClass("uify-wiki-sidebar_hidden");
      },
    });

    // CLICKING SIDEBAR WHEN HIDDEN DISPLAYS IT
    uifyWiki.$sidebar.click(function () {
      if (uifyWiki.$container.hasClass("uify-wiki-sidebar_hidden")) uifyWiki.$container.removeClass("uify-wiki-sidebar_hidden");
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REFRESH SIDEBAR
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: refresh sidebar display
    ARGUMENTS: ( <pageMaker> )
    RETURN: <void>
  */
  refreshSidebar: function (askedPage) {
    var uifyWiki = this;

    uifyWiki.$sidebarContent.empty();

    _.each(uifyWiki.options.pages, function (pageObject) {

      // subpage container
      pageObject.$sidebarContainer = uifyWiki.$sidebarContent.div("sidebar-subpage");

      // title
      pageObject.$sidebarTitle = pageObject.$sidebarContainer.a({
        class: "sidebar-title",
        htmlSanitized: pageObject.sidebarTitle || pageObject.title,
      }).click(function () {
        $$.url.query.set(pageObject.id);
        $$.url.hash.set("");
        uifyWiki.switchPage(pageObject);
      });

      // subtitles container
      pageObject.$sidebarSubtitles = pageObject.$sidebarContainer.div("sidebar-subtitles");

      // color current page
      if (pageObject.id == askedPage.id) pageObject.$sidebarTitle.addClass("current");

    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY PAGE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display asked page
    ARGUMENTS: ( <pageMaker> )
    RETURN: <void>
  */
  displayPage: function (pageObject) {
    var uifyWiki = this;

    var $pageContainer = uifyWiki.$body.div({
      class: "page_container"+ ( pageObject.class ? " " + pageObject.class : ""),
    });

    // MAKE SURE PAGE OBJECT HAS APPROPRIATTE TYPE
    if (_.isString(pageObject)) pageObject = { content: pageObject, }
    else if (_.isFunction(pageObject)) pageObject = { content: pageObject, }

    // DISPLAY PAGE LOGO
    if (pageObject.logoUrl) $pageContainer.img({
      class: "logo",
      src: pageObject.logoUrl,
    });

    // DISPLAY PAGE TITLE
    if (pageObject.title) $pageContainer.div({
      class: "title",
      htmlSanitized: pageObject.title,
    });

    // DISPLAY PAGE CONTENTS
    var $bodyContent = $pageContainer.div({
      class: "content",
      htmlSanitized: _.isFunction(pageObject.content) ? _.partial(pageObject.content, _, pageObject, uifyWiki) : pageObject.content,
    });
    if (!pageObject.content) $$log.warning("No page content for page "+ pageObject.id, pageObject);

    // DISPLAY PAGE FOOTER
    if (pageObject.footer) $pageContainer.div({
      class: "footer",
      htmlSanitized: _.isFunction(pageObject.footer) ? _.partial(pageObject.footer, _, pageObject, uifyWiki) : pageObject.footer,
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SWITCH PAGE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: switch page
    ARGUMENTS: ( <pageMaker> )
    RETURN: <void>
  */
  switchPage: function (pageObject) {
    var uifyWiki = this;

    // EMPTY BODY AND REFRESH BODY BEFORE THE REST
    uifyWiki.$body.empty();
    uifyWiki.refreshSidebar(pageObject);

    // DISPLAY PAGE DOM
    uifyWiki.displayPage(pageObject);

    // MAKE SURE TO SCROLL BACK TO TOP OF THE PAGE
    $$.dom.scrollTo(uifyWiki.$body.children(":first"), uifyWiki.$body);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REFRESH PAGE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: refresh page display (go to appropriate page) getting desired page from url query
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  refreshPage: function () {
    var uifyWiki = this;

    var query = $$.url.query.getAsString();
    if (query) uifyWiki.switchPage(_.findWhere(uifyWiki.options.pages, { id: query, }))
    else uifyWiki.switchPage(uifyWiki.options.abstract);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION:
    create a wiki page, with a sidebar listing pages and subpages on the left, and the pages content on the right
    doesn't have to fill the page if you add it to subelements
  ARGUMENTS: ({
    ?$container: <yquerjObject> « if not defined, will use window.$app or $("body") as default container »,
    ?backgroundWallpaper: <string> « path to the background wallpaper image you want to use »,
    ?logoUrl: <string> « url of logo to display in sidebar (and abstract page if not customized) »,
    ?title: <htmlString> « title to display in sidebar (and in abstract if not customized) »,
    ?sidebarFooter: <htmlString|divFillingFunction>,
    ?abstract: <pageMaker> « this object defines the basic homepage display »,
    !pages: <pageMaker[]>,
  })
  RETURN: <{
    options: <uify.wiki·options>,
    $sidebar: <yquerjObject>,
    $body: <yquerjObject>,
    initializeSidebar: <function(ø)>,
    refreshSidebar: <function(<pageMaker>)>,
    displayPage: <function(<pageMaker>)>,
    switchPage: <function(<pageMaker>)>,
    refreshPage: <function(ø)>,
    $body: <yquerjObject>,
    $sidebar: <yquerjObject>,
  }>
  TYPES:
    pageMaker = <
      | <htmlString> « the content of the page »
      | <pageContentMakerFunction> « a function creating the page content »,
      | {
          ?id: <string> «
            page deep key code, the way it is displayed in url id
            if you don't set this, the begining of the title or a random uuid will be used instead
          »,
          ?class: <string> « custom class to add to page container »,
          ?logoUrl: <string|false> « if undefined will default to options.logoUrl unless set to false »,
          ?title: <htmlString|false> «
            title of the page as displayed in the page body
            if undefined, make sure you fill sidebarTitle so that page can be displayed properly in sidebar
            if false, won't display title
          »,
          ?sidebarTitle: <htmlString> «
            title of the page as displayed in sidebar
            if undefined, will default to title
          »,
          !content: <
            | <htmlString> « the content of the page »,
            | <pageContentMakerFunction> « a function creating the page content »,
          >,
          ?footer: <htmlString> « small footer to display in bottom of abstract page »,
        }
    >
    pageContentMakerFunction = <function(
      $container <yquerjObject> « page container in which to add any content you want to display »,
      pageObject <pageMaker>,
      uifyWiki <uify.wiki·return>,
    ):<void>>
    divFillingFunction = <function(
      $container <yquerjObject> « container in which to add any content you want to display »,
      uifyWiki <uify.wiki·return>,
    ):<void>>
*/
uify.wiki = function (options) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              DEFAULTS

  var defaultOptions = {
    $container: window.$app || $("body"),
  };
  options = $$.defaults(defaultOptions, options);

  if (!options.abstract) options.abstract = {};
  else if (_.isString(options.abstract) || _.isFunction(options.abstract)) options.abstract = { content: options.abstract, };
  if (!options.abstract.title) options.abstract.title = options.title;
  if (!options.abstract.logoUrl) options.abstract.logoUrl = options.logoUrl;
  options.abstract.id = "";
  options.abstract.class = "uify-wiki-page-abstract";

  // maks sure pages objects have all defaults
  options.pages = _.map(options.pages, function (pageObj) {
    if (_.isString(pageObj) || _.isFunction(pageObj)) var finalPageObject = { content: pageObj, }
    else var finalPageObject = pageObj;
    if (!finalPageObject.id && finalPageObject.title) finalPageObject.id = _.first(finalPageObject.title.replace(/[^A-Za-z0-9]/g, ""), 10).join("");
    if (!finalPageObject.id) finalPageObject.id = $$.uuid().match(/^[^-]*/);
    return finalPageObject;
  });

  //
  //                              INITIALIZE RETURN OBJECT

  var uifyWiki = {
    options: options,
    // create sidebar and body
    $container: options.$container.addClass("uify-wiki"),
    $sidebar: options.$container.div("uify-wiki-sidebar"),
    $body: options.$container.div("uify-wiki-body"),
  };

  // HIDE SIDEBAR BY DEFAULT ON MOBILE
  if ($$.isTouchDevice()) uifyWiki.$container.addClass("uify-wiki-sidebar_hidden");

  // ADD METHODS TO WIKI OBJECT
  $$.methods(uifyWiki, uifyWikiMethods);

  // BODY CUSTOMIZATION
  if (options.backgroundWallpaper) uifyWiki.$body.css("background-image", "url(\""+ options.backgroundWallpaper +"\")");

  // INITIALIZE SIDEBAR
  uifyWiki.initializeSidebar();

  // INITIALIZE REFRESHING PAGE ON QUERY OR HASH CHANGE
  // so that if a link is clicked, page is reloaded
  $(window).on("querychange", function () { uifyWiki.refreshPage; });
  $(window).on("hashchange",  function () { uifyWiki.refreshPage; });

  //
  //                              INITIALIZE PAGE STATE AND RETURN WIKI OBJECT

  uifyWiki.refreshPage();
  return uifyWiki;

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = uify.wiki;

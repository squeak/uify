var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");
var uify = require("../core");
var Swiper = require("swiper/bundle").Swiper;
var async = require("async");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function createDomElements (options, callbackIfAsync) {

  var slideReturn = {};

  // make containers
  options.$target.addClass("uify-swiper");
  var $container = options.$target.div({ class: "uify-swiper_container swiper-container", });
  var $slidesContainer = $container.div({ class: "swiper-wrapper", });

  // make dom elements
  slideReturn.container = $container;
  slideReturn.slidesContainer = $slidesContainer;
  slideReturn.pagination = $container.div({ class: "swiper-pagination", });
  slideReturn.prev = $container.div({ class: "swiper-button-prev icon-angle-left", });
  slideReturn.next = $container.div({ class: "swiper-button-next icon-angle-right", });
  slideReturn.scrollbar = $container.div({ class: "swiper-scrollbar", });

  // make slides synchronously
  if (!options.async) slideReturn.slides = _.map(options.slides, function (slideOptions) {
    var $slide = $slidesContainer.div({ class: "uify-swiper-slide swiper-slide", });
    options.slideMaker($slide, slideOptions);
    return $slide;
  });
  // make slides asynchronously
  else {
    async.mapSeries(options.slides, function (slideOptions, next) {
      var $slide = $slidesContainer.div({ class: "uify-swiper-slide swiper-slide", });
      options.slideMaker($slide, slideOptions, next);
    }, function (err, slides) {
      slideReturn.slides = slides;
      if (callbackIfAsync) callbackIfAsync(slideReturn);
    });
  };

  // return
  return slideReturn;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function hideUselessElements (options, $dom) {
  if (!options.navigationArrows) { $dom.prev.hide(); $dom.next.hide(); }
  if (!options.scrollbar) $dom.scrollbar.hide();
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create swiping slides
  ARGUMENTS: ({
    !$target: <yquerjObject>,
    !slides: <any[]> « each will be passed to slideMaker »,
    |-- !slideMaker: <function(
          $slide <yquerjObject>,
          slide <slide>,
          next <function($slide <yquerjObject> « the created slide »)> « if async option set to true, will pass here the callback function to call when the slide is created »
        )> « determines the slides to create »,
    |-- !slidesAreImageUrls: <boolean> «
          this is a shortcut to easily display a bunch of images
          if you set this to true, you slideMaker will be generated automatically
          slides option must be an array of image urls
          if you use this option, image will automatically be previewable fullscreen by double tapping on them
        »,
    ?navigationArrows: <boolean>@default=true,
    ?pagination: <boolean>@default=true,
    ?scrollbar: <boolean>@default=false,
    ?swiperOptions: <see https://idangero.us/swiper/api/>,
    ?async: <boolean>@default=false « if true, will display slides one after another asynchronously one at a time »,
    ?asyncCallback: <function(same as return)>,
  })
  RETURN: <{
    $dom: <{
      container: <yquerjObject>,
      slides: <yquerjObject>, « if async option is used, this will be properly populated only after async finishes »
      slidesContainer: <yquerjObject>,
      pagination: <yquerjObject>,
      prev: <yquerjObject>,
      next: <yquerjObject>,
      scrollbar: <yquerjObject>,
    }>,
    swiper: <swiperObject>
  }>
  NOTES:
    If you encounter an issue that the slider doesn't slide properly, maybe it means you initiated the slider before showing your content in the page. To solve this issue, you can simply run swiper.update when you're swiper is visible, and it should work fine.
*/
uify.swiper = function (options) {

  //
  //                              MAKE SURE OPTIONS ARE OK

  var defaultOptions = {

    pagination: true,       // if you want to change this default, you should also change the below default in swiperOptions.pagination
    navigationArrows: true, // if you want to change this default, you should also change the below default in swiperOptions.navigation

    swiperOptions: {
      _recursiveOption: true,

      // // allow to zoom images with pinch gesture
      // // NOTE: doesn't seem to work
      // // even adding "swiper-zoom-container" and "swiper-zoom-target" classes in slideMaker below, maybe it's because slideMaker is making slides afterwards and swiperjs would need to see the structure when being created
      // zoom: {
      //   maxRatio: 5,
      //   toggle: true,
      // },

      // pagination dots
      pagination: options.pagination === false ? false : {
        el: '.swiper-pagination',
        clickable: true,
      },

      // navigation arrows
      navigation: options.navigationArrows === false ? false : {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },

      // scrollbar indicator
      scrollbar: options.scrollbar ? {
        el: '.swiper-scrollbar',
      } : false,

    },

  };
  options = $$.defaults(defaultOptions, options);


  //
  //                              PREFILL slides AND slidesMaker OPTIONS IF MAKING A LIST OF IMAGES

  if (options.slidesAreImageUrls) options.slideMaker = function ($container, imageUrl) {
    $container
      .div({ class: "uify-swiper-image_slide", })
      // display image as css background
      .css("background-image", 'url("'+ imageUrl +'")')
      // open image fullscreen on double click
      .dblclick(function () {
        uify.dialog.fullscreen(imageUrl);
      })
    ;
  };

  //
  //                              MAKE SURE MANDATORY OPTIONS ARE DEFINED

  $$.mandatory(options, {
    $target: $$.isJqueryElement,
    slides: _.isArray,
    slideMaker: _.isFunction,
  });

  //
  //                              GENERATE DOM AND SWIPER

  if (!options.async) {
    // CREATE DOM
    var $dom = createDomElements(options)
    // INITIALIZE SWIPER
    var swiper = new Swiper($dom.container[0], options.swiperOptions);
    // HIDE USELESS UI ELEMENTS
    hideUselessElements(options, $dom);
    // RETURN
    return {
      $dom: $dom,
      swiper: swiper,
    };
  }
  else {
    // CREATE DOM
    createDomElements(options, function ($dom) {
      // INITIALIZE SWIPER
      var swiper = new Swiper($dom.container[0], options.swiperOptions);
      // HIDE USELESS UI ELEMENTS
      hideUselessElements(options, $dom);
      // CALLBACK
      if (options.asyncCallback) options.asyncCallback({
        $dom: $dom,
        swiper: swiper,
      });
    });
  };

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = uify.swiper;

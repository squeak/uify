var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/date");
var $ = require("yquerj");
var uify = require("../core");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CREATE CALENDAR TABLE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create the table of the asked calendar month
  ARGUMENTS: (
    !$container <yquerjObject> « container for the calendar table »,
    !uifyCalendar <uify.calendar·return>,
  )
  RETURN: <void>
*/
function createCalendarTable ($container, uifyCalendar) {

  // CREATE TABLE CONTAINER
  var $table = $container.table();

  // CREATE HEAD AND BODY
  createTableHead($table, uifyCalendar);
  createTableBody($table, uifyCalendar);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CREATE TABLE HEAD
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function createTableHead ($table, uifyCalendar) {

  // CREATE THEAD CONTAINER
  var $tableHead = $table.thead();

  // CREATE TABLE HEADER WITH YEAR AND NAVIGATION BUTTONS (OPTIONALLY)
  if (uifyCalendar.options.separateYearAndMonthInHeader) {
    $table.addClass("separated_year_and_month_headers");
    var $navbarYear = $tableHead.tr();
    if (!uifyCalendar.options.disableNavigation) uify.button({
      $container: $navbarYear,
      class: "nextprev",
      icomoon: "arrow-left3",
      click: function () { uifyCalendar.change(uifyCalendar.momentYearMonth.clone().subtract(1, "year")); },
    });
    $navbarYear.th({
      class: "title",
      text: uifyCalendar.momentYearMonth.format("YYYY"),
    }).dblclick(function () {
      var newYear = prompt("Go to year:", uifyCalendar.momentYearMonth.format("YYYY"));
      if (_.isNaN(+newYear)) uify.toast.error("This is not a valid year.")
      else if (newYear) uifyCalendar.change(uifyCalendar.momentYearMonth.clone().year(newYear));
    });
    if (!uifyCalendar.options.disableNavigation) uify.button({
      $container: $navbarYear,
      class: "nextprev",
      icomoon: "arrow-right3",
      click: function () { uifyCalendar.change(uifyCalendar.momentYearMonth.clone().add(1, "year")); },
    });
  };

  // CREATE TABLE HEADER WITH MONTH (AND OPTIONALLY YEAR) AND NAVIGATION BUTTONS
  var $navbarMonth = $tableHead.tr();
  if (!uifyCalendar.options.disableNavigation) uify.button({
    $container: $navbarMonth,
    class: "nextprev",
    icomoon: "arrow-left3",
    click: function () { uifyCalendar.previous(); },
  });
  $navbarMonth.th({
    class: "title",
    text: uifyCalendar.options.separateYearAndMonthInHeader ? uifyCalendar.momentYearMonth.format("MMMM") : uifyCalendar.momentYearMonth.format("MMM YYYY"),
  }).dblclick(function () {
    if (uifyCalendar.options.separateYearAndMonthInHeader) {
      var newMonth = prompt("Go to month (number between 1 and 12):", uifyCalendar.momentYearMonth.format("MM"));
      if (_.isNaN(+newMonth)) uify.toast.error("This is not a valid month number.")
      else if (newMonth) uifyCalendar.change(uifyCalendar.momentYearMonth.clone().month(newMonth - 1));
    }
    else {
      var newYearMonth = prompt("Go to year-month (should be in the format YYYY-MM):", uifyCalendar.momentYearMonth.format("YYYY-MM"));
      if (newYearMonth) uifyCalendar.change(newYearMonth);
    };
  });
  if (!uifyCalendar.options.disableNavigation) uify.button({
    $container: $navbarMonth,
    class: "nextprev",
    icomoon: "arrow-right3",
    click: function () { uifyCalendar.next(); },
  });

  // CREATE TABLE HEADER WITH WEEK DAYS NAMES
  var daysInWeek = $$.date.moment.weekdaysMin(true); // true so that the days are displayed in locale specific order
  if (uifyCalendar.options.showWeekNumbers) daysInWeek.unshift("");
  var headHtml = "<tr>";
  _.each(daysInWeek, function (dayName) {
    headHtml += '<th>'+ dayName +'</th>';
  });
  headHtml += "</tr>";
  $tableHead.append(headHtml);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CREATE TABLE BODY
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function createTableBody ($table, uifyCalendar) {

  // CREATE TBODY CONTAINER
  var $tableBody = $table.tbody();

  // FIGURE OUT LIST OF DAYS IN THE MONTH
  var allDaysAndTheirClasses = figureOutListOfDaysForTable(uifyCalendar);

  // CREATE TABLE BODY HTML
  var bodyHtml = "";
  for (var i = 0, col = 0; i < allDaysAndTheirClasses.length; i++, col++) {

    // figure out when to create a new row
    if (col % 7 === 0) {
      col = 0;
      bodyHtml += "<tr>";
      // show week number if asked
      if (uifyCalendar.options.showWeekNumbers) {
        var weekNumber = allDaysAndTheirClasses[i][2].week();
        bodyHtml += '<td class="weeknumber" title="week number '+ weekNumber +'">'+ weekNumber +'</td>';
      };
    };

    // create cell
    bodyHtml += '<td date="'+ allDaysAndTheirClasses[i][2].format("YYYY-MM-DD") +'" class="'+ allDaysAndTheirClasses[i][1] +'">'+ allDaysAndTheirClasses[i][0] +'</td>';

    // figure out when to close row
    if (col % 7 === 6) bodyHtml += "</tr>";

  };

  // APPEND BODY HTML TO IT'S CONTAINER
  $tableBody.append(bodyHtml);

  // MAKE DATES CLICKABLE
  $tableBody.find(".available").click(function () {

    // clicked date
    var clickedDate = $(this).attr("date");

    // clickSelect date
    if (uifyCalendar.options.clickSelect === "date") {
      uifyCalendar.options.selected = clickedDate;
      uifyCalendar.initializeOptions();
      uifyCalendar.refresh();
      if (uifyCalendar.options.onDateChoose) uifyCalendar.options.onDateChoose(clickedDate, $$.date.moment(clickedDate));
    }
    // clickSelect range
    else if (uifyCalendar.options.clickSelect === "range") {
      // first click
      if (!uifyCalendar.currentlySelectingRange) {
        uifyCalendar.options.selected = [clickedDate];
        uifyCalendar.currentlySelectingRange = true;
        $tableBody.find(".selected").removeClass("selected");
        $tableBody.find(".selstart").removeClass("selstart");
        $tableBody.find(".selend").removeClass("selend");
        $(this).addClass("selected selstart")
      }
      // second click
      else {
        uifyCalendar.currentlySelectingRange = false;
        uifyCalendar.options.selected.push(clickedDate);
        uifyCalendar.initializeOptions();
        uifyCalendar.refresh();
        if (uifyCalendar.options.onDateChoose) uifyCalendar.options.onDateChoose({
          start: uifyCalendar.options.selected[0],
          end: uifyCalendar.options.selected[1],
        }, {
          start: $$.date.moment(uifyCalendar.options.selected[0]),
          end: $$.date.moment(uifyCalendar.options.selected[1]),
        });
      };
    };

    // onClickDate event
    if (uifyCalendar.options.onClickDate) uifyCalendar.options.onClickDate(clickedDate, $$.date.moment(clickedDate));

  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  LIST DAYS FOR TABLE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: make list of days for the current month table (including days of previous and next month that fit in)
  ARGUMENTS: ( !uifyCalendar <uify.calendar·return> )
  RETURN: <[
    <integer> « day number to display in table »,
    <string> « classes to apply to this day »,
    <momentObject> « the moment date of the day »,
  ][]>
*/
function figureOutListOfDaysForTable (uifyCalendar) {
  var datesInTable = [];

  // POPULATE ARRAY WITH DATES FOR THIS MONTH
  var daysInMonth = uifyCalendar.momentYearMonth.daysInMonth();
  for (var i = 1; i <= daysInMonth; i++) {
    datesInTable.push(uifyCalendar.momentYearMonth.clone().date(i));
  }

  // ADD NECESSARY DATES BEFORE START (DAYS OF PREVIOUS MONTH)
  var firstDateInTable = uifyCalendar.momentYearMonth.clone();
  while (firstDateInTable.weekday() !== 0) {
    firstDateInTable = firstDateInTable.clone().subtract(1, "day");
    datesInTable.unshift(firstDateInTable);
  }

  // ADD NECESSARY DATES AFTER END (DAYS OF NEXT MONTH)
  var lastDateInTable = datesInTable[datesInTable.length-1].clone();
  while (datesInTable.length < 42) { // 42 because there are 6 rows of seven days in table
    lastDateInTable = lastDateInTable.clone().add(1, "day");
    datesInTable.push(lastDateInTable);
  }

  // FIGURE OUT NUMBER AND CLASSES FOR EACH DATE
  var daysInTable = [];
  var monthNumber = uifyCalendar.momentYearMonth.month();
  _.each(datesInTable, function (momentDate) {

    // FIGURE OUT CLASSES FOR THIS DATE
    var classes = [];
    // is today
    if (momentDate.isSame(new Date(), "day")) classes.push("today");
    // is weekend
    if (momentDate.weekday() > 5) classes.push("weekend");
    // is other month
    if (momentDate.month() !== monthNumber) classes.push("othermonth");
    // is selected
    if (uifyCalendar.selectedRange) {
      if (momentDate.isBetween(uifyCalendar.selectedRange[0], uifyCalendar.selectedRange[1])) classes.push("selected");
      if (momentDate.format("YYYY-MM-DD") === uifyCalendar.selectedRange[0].format("YYYY-MM-DD")) classes.push("selstart");
      if (momentDate.format("YYYY-MM-DD") === uifyCalendar.selectedRange[1].format("YYYY-MM-DD")) classes.push("selend");
    };
    // is available
    if (
      (!uifyCalendar.minDate || momentDate.isAfter(uifyCalendar.minDate) ) &&
      (!uifyCalendar.maxDate || momentDate.isBefore(uifyCalendar.maxDate) )
    ) classes.push("available")
    else classes.push("unavailable");


    // POPULATE DAYSINTABLE ARRAY
    daysInTable.push([momentDate.date(), classes.join(" "), momentDate]);

  });

  return daysInTable;
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: create a calendar widget
  ARGUMENTS: ({
    ?$container: <yquerjObject>@default=$("body") « in which dom container to display the calendar »,
    ?yearMonth: <string|momentjsAcceptableDate>@default=$$.date.month() « month to show in the format "YYYY-MM", or any format that momentjs accepts »,
    ?selected: <
      |-- <momentjsAcceptableDate>
      |-- [ startDate<momentjsAcceptableDate>, endDate<momentjsAcceptableDate> ]
    > « selected date or date range »,
    ?separateYearAndMonthInHeader: <boolean>@default=false « if true, will display two headers, one with year, the second with month, making navigation easier »,
    ?minDate: <momentjsAcceptableDate> « minimal date the calendar can be set to and select »,
    ?maxDate: <momentjsAcceptableDate> « maximal date the calendar can be set to and select »,
    ?showWeekNumbers: <boolean>@default=false « if true, will show week numbers at the beginning of each week's days list »,
    ?disableNavigation: <boolean>@default=false « if true, will not allow to switch between months »,
    ?clickSelect: <"date"|"range">@default=false «
      if "date", clicking date will allow the user to select a date,
      if "range", clicking date will allow the user to select a date range,
    »,
    ?onClickDate: <function(
      <string> « the clicked date as a YYYY-MM-DD string »,
      <momentObject> « the clicked date as moment object »,
    )>,
    ?onDateChoose: <function(
      <string|{ start: <string>, end: <string> }> « the clicked date/range as a YYYY-MM-DD string(s) »,
      <string|{ start: <momentObject>, end: <momentObject> }> « the clicked date/range as moment object(s) »,
    )> «
      similar to onClickDate, but is only ran when clickSelect is enabled,
      if clickSelect=="date" fires every time a date is clicked,
      if clickSelect=="range" fires only when second date is clicked and ranged is formed,
    »,
    ?onMonthChange: <function(
      <string> « the newly shown month as a YYYY-MM string »,
      <momentObject> « the newly shown month as moment object »,
    )>,
    ?onRefresh: <function(ø){@this=<uifyCalendar>}> « fired when calendar view is refreshed »,
  })
  RETURN: <{
    options: <uify.calendar·options> « the options passed when creating the calendar »,
    $container: <yquerjObject> « dom element in which the calendar has been created »,
    $el: <yquerjObject> « the calendar dom element »,
    momentYearMonth: <momentObject> « the moment object representing the currently displayed month »,
    selectedRange: <momentObject[]|undefined> « the currently selected date or range of dates »,
    setSelected: <function(dateOrRangeToSelect <uify.calendar·options.selected>)> « trigger the modification of the selected date or range »,
    previous: <function(ø)> « switch calendar to previous month »,
    next: <function(ø)> « switch calendar to next month »,
    change: <function(
      <momentjsAcceptableDate> « object or string specifying the new year and month to use »
    )> « switch calendar to the given month »,
  }>
  EXAMPLES:
    uify.calendar({
      $container: $("#some-dom-element"),
      yearMonth: "2001-01",
    });
*/
uify.calendar = function (options) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DEFAULT OPTIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  var defaultOptions = {
    $container: $("body"),
    yearMonth: $$.date.month(),
  };
  options = $$.defaults(defaultOptions, options);

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CREATE UIFY CALENDAR OBJECT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  var uifyCalendar = {
    $container: options.$container,
    options: options,

    // get month as moment object and make sure date of month is set to first day in month at 12:00:00
    // chosenMonth <momentjsAcceptableDate>
    setSelectedMonthMoment: function (chosenMonth) {
      uifyCalendar.options.yearMonth = chosenMonth;
      uifyCalendar.momentYearMonth = $$.date.moment(chosenMonth).date(1).hour(12).minute(0).second(0);
    },

    // INITIALIZE OPTIONS
    initializeOptions: function () {

      // initialize selected range
      if (_.isArray(uifyCalendar.options.selected)) uifyCalendar.selectedRange = [
        $$.date.moment(uifyCalendar.options.selected[0]).hour(0).minute(0).second(0),
        $$.date.moment(uifyCalendar.options.selected[1]).hour(23).minute(59).second(59),
      ]
      else if (!uifyCalendar.options.selected) uifyCalendar.selectedRange = undefined
      else uifyCalendar.selectedRange = [
        $$.date.moment(uifyCalendar.options.selected).hour(0).minute(0).second(0),
        $$.date.moment(uifyCalendar.options.selected).hour(23).minute(59).second(59),
      ];
      // make sure that selecteRange is a valid range
      if (uifyCalendar.selectedRange && uifyCalendar.selectedRange[0]._d > uifyCalendar.selectedRange[1]._d) uifyCalendar.selectedRange = undefined;

      // initialize accepted date range
      uifyCalendar.minDate = uifyCalendar.options.minDate ? $$.date.moment(uifyCalendar.options.minDate).hour(0).minute(0).second(0) : undefined;
      uifyCalendar.maxDate = uifyCalendar.options.maxDate ? $$.date.moment(uifyCalendar.options.maxDate).hour(23).minute(59).second(59) : undefined;

    },

    // CHANGE DISPLAYED MONTH
    previous: function () { uifyCalendar.change(uifyCalendar.momentYearMonth.clone().subtract(1, "month")); },
    next: function () { uifyCalendar.change(uifyCalendar.momentYearMonth.clone().add(1, "month")); },
    change: function (newMonth) {

      // cancel change if new month is out of available range
      var newMonthDate = $$.date.moment(newMonth);
      var newMonthStartDate = $$.date.moment(newMonth).date(1).hour(0).minute(0).second(0);
      var newMonthEndDate = $$.date.moment(newMonth).date(newMonthDate.daysInMonth()).hour(23).minute(59).second(59);
      if (
        (uifyCalendar.minDate && newMonthEndDate.isBefore(uifyCalendar.minDate)) ||
        (uifyCalendar.maxDate && newMonthStartDate.isAfter(uifyCalendar.maxDate))
      ) return;

      // refresh ui
      uifyCalendar.setSelectedMonthMoment(newMonth);
      uifyCalendar.refresh();

      // fire month changed event
      if (uifyCalendar.options.onMonthChange) uifyCalendar.options.onMonthChange(newMonthDate.format("YYYY-MM"), newMonthDate);

    },

    // CREATE/RECREATE CALENDAR DISPLAY
    refresh: function () {
      // empty body in case recreating calendar
      uifyCalendar.$el.empty();
      // create calendar in dom
      createCalendarTable(uifyCalendar.$el, uifyCalendar);
      // fire refresh event
      if (uifyCalendar.options.onRefresh) uifyCalendar.options.onRefresh.call(uifyCalendar);
    },

    // SELECT DATE(S)
    setSelected: function (dateOrRangeToSelect) {
      uifyCalendar.options.selected = dateOrRangeToSelect;
      uifyCalendar.initializeOptions();
      uifyCalendar.refresh();
    },

  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CREATE CALENDAR AND RETURN
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // CREATE CALENDAR CONTAINERS
  uifyCalendar.$el = uifyCalendar.options.$container.div({ class: "uify-calendar", });

  // SETUP CALENDAR NECESSARY VARIABLES
  uifyCalendar.initializeOptions();
  uifyCalendar.setSelectedMonthMoment(uifyCalendar.options.yearMonth);

  // CREATE CALENDAR
  uifyCalendar.refresh();

  // RETURN CALENDAR OBJECT
  return uifyCalendar;

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

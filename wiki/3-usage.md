# Usage

Load and use uify library with:

```javascript
const uify = require("uify");
uify.confirm(options);
```

### Extensions and plugins are not imported by default when you import uify and you should import them manually:

Import extensions with: `require(uify/extension/<method>)`, plugins with : `require(uify/plugin/<method>)`.
For example:

```javascript
const uify = require("uify");
require("uify/extension/swiper");
uify.swiper(options);
```
or
```javascript
const uifySwiper = require("uify/extension/swiper");
uifySwiper(options);
```

var uify = require("../../../core");
require("../../../extension/calendar");

window.demoStart = function (utilities) {

  utilities.section({
    title: "uify.calendar",
    description: "Create an interactive calendar.",
    demo: function ($container) {

      uify.calendar({
        $container: $container,
        clickSelect: "date",
        showWeekNumbers: true,
        onDateChoose: function (date) {
          uify.toast("You clicked the date: "+ date);
        },
        onMonthChange: function (newMonth) {
          uify.toast.success("You passed to month: "+ newMonth);
        },
      });

    },

  });

};

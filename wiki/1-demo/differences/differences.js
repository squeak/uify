var uify = require("../../../core");
require("../../../extension/differences");

window.demoStart = function (utilities) {

  utilities.section({
    title: "uify.differences",
    description: "Display differences between two passed objects",
    demo: function ($container) {

      //
      //                              EXAMPLE OBJECTS

      var before = {
        a: 1,
        b: 2,
        c: 3,
        d: [4, 5, 6, 7],
        e: {
          f: 8,
          g: 9,
          h: 10,
          j: [
            { k: 11, l: 12, m: 13, },
            { n: 14, o: 15, p: 16, },
          ],
        },
      };

      var after = {
        a: "some",
        b: 2,
        c: 3,
        d: [4, 5, 6, "things", 7],
        e: {
          f: 8,
          g: 9,
          h: 10,
          intro: "duced",
          j: [
            { k: 11, l: 12, m: "modified", },
            { n: 14, o: 15, p: 16, },
            "and added",
          ],
        },
      };

      //
      //                              UIFY.DIFFERENCES

      $container.div({
        class: "intertitle",
        text: "uify.differences",
      });

      uify.differences({
        $container: $container,
        before: before,
        after: after,
      });

      //
      //                              UIFY.DIFFERENCES.CONFIRM

      $container.div({
        class: "intertitle",
        text: "uify.differences.confirm",
      });

      $container.div({
        class: "button",
        text: "save",
      }).click(function () {
        uify.differences.confirm({
          text: "Save modifications?",
          before: before,
          after: after,
          displayBefore: true,
          displayAfter: true,
          cancel: function () { uify.toast.warning("Aborted saving!"); },
          ok: function () { uify.toast.success("Confirmed saving!"); },
        });
      });

      //                              ¬
      //

    },

  });

};

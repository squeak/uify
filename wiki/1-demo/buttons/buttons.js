var uify = require("../../../core");

window.demoStart = function (utilities) {

  utilities.section({
    title: "uify.button, uify.menu, uify.fab, uify.item, uify.list",
    description: "Open a simple dialog",
    demo: function ($container) {

      $container.css({
        position: "relative",
        minHeight: "200px",
      });

      //
      //                              BUTTON

      var selectableButtonSelected;
      uify.button({
        $container: $container,
        icomoon: "external-link",
        title: "open menu",
        inlineTitle: true,
        invertColor: true,
        css: {
          fontSize: "1.5em",
          maxWidth: "50%",
          marginLeft: "25%",
        },
        clickMenu: {
          buttons: [
            {
              icomoon: "heartbeat",
              title: "hello world",
              click: function () {
                uify.toast({
                  message: "you clicked the heart",
                  color: $$.random.color(1),
                });
              },
            },
            {
              icomoon: "trash",
              title: "remove?",
            },
            {
              icomoon: "checkmark4",
              icomoonClicked: "checkmark3",
              title: "selectable button",
              clicked: function () { return selectableButtonSelected },
              clickSelect: function () { selectableButtonSelected = true; uify.toast.success("you selected menu button"); },
              clickUnselect: function () { selectableButtonSelected = false; uify.toast.warning("you unselected menu button"); },
            },
          ],
          click: function (buttonOpts) {
            uify.toast({
              message: "you clicked something else than the heart: "+ buttonOpts.title,
              color: $$.random.color(),
            });
          },
        },
      });

      //
      //                              FAB

      uify.fab({
        $container: $container,
        icomoon: "plus",
        click: function () { list.addItem({ avatar: "file", text: "new file", }); },
      });

      uify.fab({
        $container: $container,
        icomoon: "moon",
        position: {
          top: "default",
          left: "stick",
        },
        click: function () { alert("you clicked the moon"); },
      });

      //
      //                              LIST OF ITEMS

      var list = uify.list({
        $container: $container,
        title: "some list of items",
        items: [
          {
            avatar: "folder",
            text: "hello",
            buttons: [
              {
                icomoon: "rocket",
                click: function () { alert("3.. 2.. 1.."); }
              },
              {
                icomoon: "ambulance",
                click: function () { alert(); }
              },
              {
                icomoon: "minus",
                click: function () { alert("hey"); }
              },
            ],
          }
        ],
      });

      //                              ¬
      //

    },

  });

};

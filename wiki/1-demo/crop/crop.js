var uify = require("../../../core");
require("../../../plugin/crop");

window.demoStart = function (utilities) {

  utilities.section({
    title: "uify.crop",
    description: "Crop an image with a nice and customizable interface",
    demo: function ($container) {

      //
      //                              RECANGULAR CROP

      uify.button({
        $container: $container,
        css: { fontSize: "2em", },
        icomoon: "crop",
        inlineTitle: "right",
        title: "rectangular crop",
        click: function () {

          uify.crop({
            image: "/share/logos/uify.png",
            callback: function (buffer) {
              console.log(buffer);
            },
          });

        },
      });

      //
      //                              FREE CROP

      uify.button({
        $container: $container,
        css: { fontSize: "2em", },
        icomoon: "crop",
        inlineTitle: "right",
        title: "free crop",
        click: function () {

          uify.crop({
            image: "/share/assets/uify/baikal.jpg",
            shape: "free",
            callback: function (buffer) {
              console.log(buffer);
            },
          });

        },
      });

      //
      //                              SQUARE CROP

      uify.button({
        $container: $container,
        css: { fontSize: "2em", },
        icomoon: "crop",
        inlineTitle: "right",
        title: "square crop",
        click: function () {

          uify.crop({
            image: "/share/logos/uify.png",
            ratio: 1,
            callback: function (buffer) {
              console.log(buffer);
            },
          });

        },
      });

      //
      //                              16:9 CROP

      uify.button({
        $container: $container,
        css: { fontSize: "2em", },
        icomoon: "crop",
        inlineTitle: "right",
        title: "16:9 crop",
        click: function () {

          uify.crop({
            image: "/share/assets/uify/baikal.jpg",
            ratio: 16/9,
            callback: function (buffer) {
              console.log(buffer);
            },
          });

        },
      });

      //                              ¬
      //

    },

  });

};

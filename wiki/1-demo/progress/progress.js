var uify = require("../../../core");

window.demoStart = function (utilities) {

  utilities.section({
    title: "uify.spinner, uify.progress",
    description: "Miscelaneous utilities",
    demo: function ($container) {

      //
      //                              SPINNER

      var spinners = [];

      $container.div({
        class: "button",
        text: "create spinner",
      }).click(function () {
        var spinner = uify.spinner({ text: "some text", });
        spinner.$el.css("color", $$.random.color(1));
        spinners.push(spinner);
      });

      $container.div({
        class: "button",
        text: "destroy spinner",
      }).click(function () {
        if (spinners[0]) {
          spinners[0].destroy();
          spinners.splice(0, 1);
        };
      });

      //
      //                              PROGRESS

      $container.div({
        class: "button",
        text: "create progress dialog",
      }).click(function () {
        var progressified = uify.progress({
          title: "Wait a bit, this is proceeding!",
          steps: $$.random.integer(30, 150),
        });
        var interval = setInterval(function () {
          progressified.refresh($$.random.integer(1, 10));
          if (progressified.count >= progressified.options.steps) clearInterval(interval);
        }, 300);
      });

      $container.div({
        class: "button",
        text: "create progress toast",
      }).click(function () {
        var progressified = uify.progress({
          title: "Wait a sec!",
          steps: $$.random.integer(200, 320),
          style: "toast",
        });
        var interval = setInterval(function () {
          progressified.refresh();
          if (progressified.count >= progressified.options.steps) clearInterval(interval);
        }, 10);
      });

      //                              ¬
      //

    },

  });

};

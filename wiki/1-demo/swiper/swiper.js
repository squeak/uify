var uify = require("../../../core");
require("../../../extension/swiper");

window.demoStart = function (utilities) {

  utilities.section({
    title: "uify.swiper",
    description: "Create swipable content, for example here, a list of logos.",
    demo: function ($container) {

      uify.swiper({
        $target: $container,
        slides: ["uify", "electrode", "squyntax", "stylectrode", "yquerj", "squeak"],
        navigationArrows: true,
        scrollbar: true,
        slideMaker: function ($slider, slide, next) {
          $slider.div().css({
            backgroundImage: 'url("/share/logos/'+ slide +'.png")',
            height: "200px",
            backgroundRepeat: "no-repeat",
            backgroundSize: "contain",
            backgroundPosition: "center",
          });
        },
      });

    },

  });

};

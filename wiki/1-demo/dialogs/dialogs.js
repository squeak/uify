var uify = require("../../../core");

window.demoStart = function (utilities) {

  utilities.section({
    title: "uify.dialog, uify.alert, uify.confirm, uify.prompt, uify.toast",
    description: "Open simple dialogs",
    demo: function ($container) {

      //
      //                              ALERTS

      $container.div({
        class: "intertitle",
        text: "uify.alert",
      });

      $container.div({
        class: "button",
        text: "open alert",
      }).click(function () {
        uify.alert({
          title: "alert title",
          content: "a simple alert",
        });
      });

      _.each(["success", "warning", "error"], function (type) {
        $container.div({
          class: "button",
          text: "open "+ type +" alert",
        }).click(function () {
          uify.alert({
            title: type +" alert",
            content: type +" "+ type +" "+ type,
            type: type,
          });
        });
      });

      //
      //                              CONFIRMS

      $container.div({
        class: "intertitle",
        text: "uify.confirm",
      });

      $container.div({
        class: "button",
        text: "open confirm",
      }).click(function () {

        uify.confirm({
          title: "confirm title",
          content: function ($container) {
            uify.button({
              $container: $container,
              icomoon: "rocket",
              css: {
                fontSize: "5em",
              },
              click: function () { uify.toast.success("hello"); },
            });
          },
        });

      });

      //
      //                              DIALOGS

      $container.div({
        class: "intertitle",
        text: "uify.dialog",
      });

      $container.div({
        class: "button",
        text: "open simple dialog",
      }).click(function () {

        uify.dialog({
          title: "dialog title",
          content: "a basic text",
          ok: function () { uify.toast.success("you validated"); },
          cancel: function () { uify.toast.warning("you canceled"); },
        });

      });

      $container.div({
        class: "button",
        text: "open complex dialog",
      }).click(function () {

        uify.dialog({
          title: "dialog title",
          content: function ($container) {
            $container.div({ text: "my customized content", });
            $container.div({ text: "on multiple lines", });
            $container.div({ text: "with a little button", });
            uify.button({
              $container: $container,
              icomoon: "heartbeat",
              click: function () {
                uify.toast({
                  message: "youhou",
                  color: $$.random.color(1),
                });
              },
            });
          },
          type: $$.random.entry(["success", "warning", "error"]),
        });

      });

      //
      //                              TOASTS

      $container.div({
        class: "intertitle",
        text: "uify.toast",
      });

      $container.div({
        class: "button",
        text: "create toast message",
      }).click(function () {

        var type = $$.random.entry(["success", "warning", "error"]);
        uify.toast[type]("¡¡¡"+ type +" toast message!!!");

      });

      $container.div({
        class: "button",
        text: "create custom color toast message",
      }).click(function () {
        uify.toast({
          message: "toast with a custom color",
          color: $$.random.color(1),
        });
      });

      $container.div({
        class: "button",
        text: "create toast that will stay until clicked",
      }).click(function () {
        uify.toast({
          message: "toast with a custom color",
          color: "white",
          duration: 0,
        });
      });

      //                              ¬
      //

    },

  });

};

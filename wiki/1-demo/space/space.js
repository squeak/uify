var uify = require("../../../core");

window.demoStart = function (utilities) {

  utilities.section({
    title: "uify.spaces, uify.panel, uify.tabs",
    description: "Create different spaces in your page, panels, tabs displays",
    demo: function ($container) {

      $container.css({
        position: "relative",
        minHeight: "300px",
      });

      //
      //                              SPACES

      var $spacesContainer = $container.div().css({
        width: "300px",
        height: "200px",
        background: "peachpuff",
        textAlign: "center",
      });

      uify.spaces({
        $target: $spacesContainer,
        spaces: [
          {
            title: "one",
            contentMaker: function ($container) { $container.div({ text: "——— space one ———"}) }
          },
          {
            title: "two",
            contentMaker: function ($container) { $container.div({ text: "——— space two ———"}) }
          },
        ]
      });

      //
      //                              PANEL WITH TABS

      var panel;
      uify.button({
        $container: $container,
        icomoon: "layout6",
        css: { fontSize: "5em", display: "inline-block", },
        clickSelect: function () {
          if (!panel) makePanel()
          else panel.show();
        },
        clickUnselect: function () {
          panel.hide();
        },
      });

      function makePanel () {
        panel = uify.panel({
          $container: $container,
          title: "panel title",
          tabs: [
            {
              title: "tab 1",
              content: function ($container) { $container.div({ text: "——— tab one ———"}) },
            },
            {
              title: "tab 2",
              content: function ($container) { $container.div({ text: "——— tab two ———"}) },
            },
          ],
        });
        panel.$el.css({ color: "white", });
      };

      //                              ¬
      //

    },

  });

};

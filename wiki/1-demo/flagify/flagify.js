var uify = require("../../../core");
require("../../../plugin/flagify");

window.demoStart = function (utilities) {

  utilities.section({
    title: "uify.flagify",
    description: "Show flag of the given country",
    demo: function ($container) {

      //
      //                              ARGENTINA

      $container.div({
        class: "intertitle",
        text: "Argentina's flag",
      });
      $container.div({
        htmlSanitized: uify.flagify("Argentina"),
      });

      //
      //                              RANDOM FLAG

      var $randomFlag;
      $container.div({
        class: "intertitle",
        text: "Random flag",
      });
      uify.button({
        $container: $container,
        title: "Show random flag",
        inlineTitle: true,
        invertColor: true,
        click: function () {
          if ($randomFlag) $randomFlag.remove();
          $randomFlag = $container.div({
            htmlSanitized: uify.flagify($$.random.entry(uify.flagify.list).Country_name),
          });
        },
      });


      //                              ¬
      //

    },

  });

};

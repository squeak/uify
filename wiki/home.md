
A library with basic utilities to create UI elements.
Similar to jqueryUI, uify proposes a set of methods to create UI pieces, but without relying on the jquery object, while still supporting jquery elements.

For example, you could create a nice looking button, that when clicked opens a dialog that will fetch a user's profile with the following code:
```js
var button = uify.button({
  $container: $("body"),
  icomoon: "profile",
  title: "click to fetch your profile",
  click: function () {

    var dialog = uify.dialog({
      title: "My profile",
      content: function ($contentContainer) {

        var spinner = uify.spinner({
          $container: $contentContainer,
          text: "loading your profile..."
        });

        fetchUserProfile(function (username, userDescription) {
          spinner.destroy();
          $contentContainer.div({
            class: "username",
            text: username,
          });
          $contentContainer.div({
            class: "description",
            text: userDescription,
          });
        });

      },
    })

  },
})
```


function meth (methodName) {
  return {
    name: "uify."+ methodName,
    path: "extension/"+ methodName,
  };
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {

  display: {
    title: "EXTENSION METHODS",
    commment: 'Extensions, you must import them with <pre>require("uify/extension/&ltextensionName&gt")</pre>.',
    borderColor: "secondary",
  },

  methods: [
    meth("calendar"),
    {
      name: "uify.differences",
      path: "extension/differences",
      methodsAutofill: {
        type: "all",
        excluded: ["uify.differences.uify.differences"],
      },
    },
    meth("preview"),
    meth("swiper"),
    meth("wiki"),
  ],

};


module.exports = {

  display: {
    title: "PLUGIN METHODS",
    commment: 'Plugins, you must import them with <pre>require("uify/plugin/&ltpluginName&gt")</pre>.',
    borderColor: "tertiary",
  },

  methods: [

    // CROP
    {
      name: "uify.crop",
      path: "plugin/crop",
    },

    // FLAGIFY
    {
      name: "uify.flagify",
      path: "plugin/flagify",
      methodsAutofill: {
        type: "all",
        excluded: ["uify.flagify.uify.flagify"],
      },
    },
  ],

};

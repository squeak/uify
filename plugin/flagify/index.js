var _ = require("underscore");
var $$ = require("squeak");
var uify = require("../../core");
var countries = require("./countries.json");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: get a span showing the flag of the asked country
  ARGUMENTS: (
    ?countryName <string>,
    ?options <{
      ?silenceErrors: <boolean>@default=false « if true, will not log an error if country is not found »,
      ?size: <"xs"|"s"|"m"|"l"|"xl">,
    }>,
  )
  RETURN: <htmlString>
*/
uify.flagify = $$.scopeFunction({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: list of all countries
    TYPE: <countryObject[]>
    TYPES:
      countryObject = <{
        Code: <string>,
        Country_name: <string>,
        Year: <string>,
        ccTLD: <string>,
        ISO 3166-2: <string>,
        Notes: <string>,
      }>
  */
  list: countries,

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  main: function (countryName, options) {
    if (!options) options = {};
    var countryObject = uify.flagify.getCountryObject(countryName, options.silenceErrors);
    var sizeClass = options.size ? " uify-flag-size_"+ options.size : "";
    return '<span class="uify-flag uify-flag-'+ countryObject.Code + sizeClass +'" title="'+ countryObject.Country_name +'"></span>';
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get the object of a country
    ARGUMENTS: (
      ?countryName <string>,
      ?silenceErrors <boolean>@default=false « if true, will not log an error if country is not found »,
    )
    RETURN: <countryObject>
  */
  getCountryObject: function (countryName, silenceErrors) {
    var countryObject = _.findWhere(uify.flagify.list, { Country_name: countryName, });
    if (!countryObject) {
      if (!silenceErrors) $$.log.error("[uify] Cannot find country named '"+ countryName +"'. Used default flag.")
      countryObject = _.findWhere(uify.flagify.list, { Country_name: "", });
    };
    return countryObject;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});

module.exports = uify.flagify;

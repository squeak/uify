var _ = require("underscore");
var $$ = require("squeak");
var uify = require("../../core");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display a dialog with spaces, buttons...
  ARGUMENTS: ({
    ?title: <string> « title for the crop dialog »,
    ?subtitle: <string> « subtitle for the crop dialog »,
    !displayedCallback: <function(drawSpace <uify.space·result>, visualizeSpace <uify.space·result>)>,
    !clearInterface: <function(ø)>,
    !cropProcess: <function(ø)>,
    !save: <function(<Blob>),
  })
  RETURN: <void>
*/
module.exports = function (opts) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  var spaced, drawSpace, visualizeSpace;

  var dialog = uify.dialog({

    //
    //                              GENERAL

    title: opts.title,
    subtitle: opts.subttitle,

    height: 90,
    width: 90,
    overlayClickExits: false,
    cssContent: {
      width: "calc(100% - 1em)",
      height: "100%",
    },

    //
    //                              BUTTONS

    buttons: [
      {
        name: "full",
        title: "select <u>f</u>ull",
        key: "f",
        click: function () {
          opts.selectFull();
          return false;
        },
      },
      {
        name: "clear",
        title: "cl<u>e</u>ar selection",
        key: "e",
        click: function () {
          opts.clearInterface();
          return false;
        },
      },
      {
        name: "crop",
        title: "<u>c</u>rop",
        key: "c",
        click: function () {
          if (!opts.readyToCrop()) return false;
          opts.cropProcess();
          visualizeSpace.visit();
          dialog.hideButton("full");
          dialog.hideButton("clear");
          dialog.hideButton("crop");
          dialog.showButton("back");
          dialog.showButton("ok");
          return false;
        },
      },
      {
        name: "back",
        title: "back",
        key: "backspace",
        condition: false,
        click: function () {
          drawSpace.visit();
          dialog.showButton("full");
          dialog.showButton("clear");
          dialog.showButton("crop");
          dialog.hideButton("back");
          dialog.hideButton("ok");
          return false;
        },
      },
      {
        name: "ok",
        title: "ok",
        key: "enter",
        condition: false,
        click: function () {
          opts.save();
        },
      },
    ],

    //
    //                              CONTENT

    content: function ($container) {

      spaced = uify.spaces({
        $target: $container,
        hideLeftArrow: true,
        hideRightArrow: true,
      });
      drawSpace = spaced.create();
      visualizeSpace = spaced.create(undefined, true);

      opts.displayedCallback(drawSpace, visualizeSpace);

    },

    //
    //                              CANCEL

    cancel: function () {
      if (!confirm("Abandon editing image?")) return false;
    },

    //                              ¬
    //

  });

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/array");
var interfaceMaker = require("../utilities/interface");
var drawCircles = require("../utilities/drawCircles");
var movePoint = require("../utilities/movePoint");
var createPoint = require("../utilities/createPoint");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  LET THE USER DRAW ON THE CANVAS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function drawOnCanvas (constrainedRatio, $canvas, shapeChangedCallback) {
  var workZone = interfaceMaker({

    //
    //                              GENERAL

    $canvas: $canvas,
    shapeChanged: shapeChangedCallback,

    //
    //                              DRAW POLYGON AND CIRCLES

    draw: function (workZone, e) {

      // draw polygon and circles
      var lineThickness = workZone.getLineThickness();
      if (workZone.pointsList.length > 1) drawRectangle({
        canvas: workZone.canvas,
        context: workZone.context,
        pointsList: workZone.pointsList,
        lineThickness: lineThickness,
        ratio: constrainedRatio,
        event: e,
      });
      drawCircles(workZone.context, workZone.pointsList, lineThickness, workZone.getCircleScaledRadius());

    },

    //
    //                              ADD, MOVE POINT

    clickedSurface: function (e, clickCoordinates) {
      if (workZone.pointsList.length < 2) createPoint(workZone, clickCoordinates, e);
    },

    movePoint: function (e, clickCoordinates) {
      movePoint(workZone, clickCoordinates, e);
    },

    //                              ¬
    //

  });
  return workZone;
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE SQUARE SHAPE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function recalculateSecondPointPositionToFitRatio (pointsList, ratio, canvas) {

  var width = Math.abs(pointsList[1].x - pointsList[0].x);
  var newHeight = width / ratio;

  if (pointsList[0].x < pointsList[1].x) pointsList[1].y = pointsList[0].y + newHeight
  else pointsList[1].y = pointsList[0].y - newHeight

  if (pointsList[1].y > canvas.height) {
    var oversizeY = pointsList[1].y - canvas.height;
    pointsList[1].y = canvas.height;
    pointsList[1].x = pointsList[1].x - oversizeY * ratio;
  };

  if (pointsList[1].y < 0) {
    var oversizeY = pointsList[1].y;
    pointsList[1].y = 0;
    pointsList[1].x = pointsList[1].x - oversizeY * ratio;
  };

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DRAW POLYGON
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function drawRectangle (options) {

  options.context.lineWidth = options.lineThickness;
  options.context.strokeStyle = "orange";

  // recalculate second point's position to make a proper square
  if (options.ratio) recalculateSecondPointPositionToFitRatio(options.pointsList, options.ratio, options.canvas)
  else if (options.event.shiftKey) recalculateSecondPointPositionToFitRatio(options.pointsList, 1, options.canvas);

  options.context.beginPath();
  options.context.moveTo(options.pointsList[0].x, options.pointsList[0].y);
  _.each([
    {
      x: options.pointsList[0].x,
      y: options.pointsList[1].y,
    },
    {
      x: options.pointsList[1].x,
      y: options.pointsList[1].y,
    },
    {
      x: options.pointsList[1].x,
      y: options.pointsList[0].y,
    },
  ], function (coordinates) {
    options.context.lineTo(coordinates.x, coordinates.y);
  });
  options.context.closePath();
  options.context.stroke();

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = drawOnCanvas;

var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (img, canvasSource, canvasDestination, cropPolygon) {

  // X
  if (cropPolygon[0].x < cropPolygon[1].x) {
    var topLeftX = cropPolygon[0].x;
    var bottomRightX = cropPolygon[1].x;
  }
  else {
    var topLeftX = cropPolygon[1].x;
    var bottomRightX = cropPolygon[0].x;
  };

  // Y
  if (cropPolygon[0].y < cropPolygon[1].y) {
    var topLeftY = cropPolygon[0].y;
    var bottomRightY = cropPolygon[1].y;
  }
  else {
    var topLeftY = cropPolygon[1].y;
    var bottomRightY = cropPolygon[0].y;
  };

  // WIDTH AND HEIGHT
  var imageWidth = bottomRightX - topLeftX;
  var imageHeight = bottomRightY - topLeftY;

  // GET DESTINATION CANVAS CONTEXT, CLEAN IT, RESIZE IT
  canvasDestination.width = imageWidth;
  canvasDestination.height = imageHeight;
  var destinationContext = canvasDestination.getContext("2d");
  destinationContext.clearRect(0, 0, destinationContext.canvas.width, destinationContext.canvas.height);

  // DRAW RESULT IMAGE
  destinationContext.drawImage(
    img,
    // start x and y
    topLeftX, topLeftY,
    // get this width and height
    imageWidth, imageHeight,
    // put at this position in new image
    0, 0,
    // with the following width and height
    imageWidth, imageHeight,
  );

};

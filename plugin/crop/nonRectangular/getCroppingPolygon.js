var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: from an unorgannized list of points, get the 4 corners
  ARGUMENTS: (
    !unorganizedPointsList <{ x: <number>, y: <number>, }[]> « should be at least of lengths four if you want proper output »,
  )
  RETURN: <{
    TL: <{ x: <number>, y: <number>, },
    TR: <{ x: <number>, y: <number>, },
    BR: <{ x: <number>, y: <number>, },
    BL: <{ x: <number>, y: <number>, },
  }>
*/
function getCornersCoordinatesFromUnorgannizedList (unorganizedPointsList) {

  // store indexes in coordinates
  _.each(unorganizedPointsList, function (coordinates, index) { coordinates.index = index; });

  // create sorted list (by x and by y)
  var sortedByX = _.sortBy(unorganizedPointsList, "x");
  var sortedByY = _.sortBy(unorganizedPointsList, "y");

  // create a rating of topLeftedness for each point depending of it's index in sorted by x and by y lists
  var length = unorganizedPointsList.length -1; // four corners minus one
  _.each(sortedByX, function (coordinates, index) {
    coordinates.positionsRatings = {
      TL: index,
      TR: length - index,
      BL: index,
      BR: length - index,
    };
  });
  _.each(sortedByY, function (coordinates, index) {
    coordinates.positionsRatings = {
      TL:     coordinates.positionsRatings.TL + index,
      TR:    coordinates.positionsRatings.TR + index,
      BL:  coordinates.positionsRatings.BL + length - index,
      BR: coordinates.positionsRatings.BR + length - index,
    };
  });

  // return the index of point with smallest topLeftedness rating
  var polygonOrgannizedObject = {};
  var indexWasUsed = {};
  for (i=0; i<6; i++) {
    _.each(unorganizedPointsList, function (coordinates) {
      _.each(coordinates.positionsRatings, function (rating, positionName) {
        if (polygonOrgannizedObject[positionName]) return
        else if (indexWasUsed[coordinates.index]) return
        else if (rating == i) {
          polygonOrgannizedObject[positionName] = coordinates;
          indexWasUsed[coordinates.index] = true;
        };
      });
    });
  };

  // return polygon four corners
  return polygonOrgannizedObject;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = getCornersCoordinatesFromUnorgannizedList;

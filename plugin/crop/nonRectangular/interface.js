var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/array");
var interfaceMaker = require("../utilities/interface");
var drawCircles = require("../utilities/drawCircles");
var movePoint = require("../utilities/movePoint");
var createPoint = require("../utilities/createPoint");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  LET THE USER DRAW ON THE CANVAS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function drawOnCanvas ($canvas, shapeChangedCallback) {
  var workZone = interfaceMaker({

    //
    //                              GENERAL

    $canvas: $canvas,
    shapeChanged: shapeChangedCallback,

    //
    //                              DRAW POLYGON AND CIRCLES

    draw: function (workZone) {

      // draw polygon and circles
      var lineThickness = workZone.getLineThickness();
      drawPolygon(workZone.context, workZone.pointsList, lineThickness);
      drawCircles(workZone.context, workZone.pointsList, lineThickness, workZone.getCircleScaledRadius());

    },

    //
    //                              ADD, REMOVE, MOVE POINT

    clickedPoint: function (e, clickCoordinates) {
      if (e.shiftKey) removePoint(workZone, e);
    },

    clickedSurface: function (e, clickCoordinates) {
      createPoint(workZone, clickCoordinates, e);
    },

    movePoint: function (e, clickCoordinates) {
      movePoint(workZone, clickCoordinates, e);
    },

    //                              ¬
    //

  });
  return workZone;
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  REMOVE POINT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function removePoint (workZone, e) {

  // remove point from list
  $$.array.removeByValue(workZone.pointsList, workZone.currentlyClickedPoint);

  // redraw everything
  workZone.draw(e);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DRAW POLYGON
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function drawPolygon (context, pointsList, lineThickness) {

  context.lineWidth = lineThickness;
  context.strokeStyle = "orange";

  context.beginPath();
  context.moveTo(pointsList[0].x, pointsList[0].y);
  _.each(_.rest(pointsList), function (coordinates) {
    context.lineTo(coordinates.x, coordinates.y);
  });
  context.closePath();
  context.stroke();

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = drawOnCanvas;

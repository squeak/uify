var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/array");
var getCroppingPolygon = require("./getCroppingPolygon");
var distance = require("../utilities/distance");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  FIGURE OUT DESTINATION IMAGE DIMENSIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function figureOutDestinationImageDimensions (cropPolygon) {

  var top = distance(cropPolygon.TL, cropPolygon.TR);
  var bottom = distance(cropPolygon.BL, cropPolygon.BR);
  var left = distance(cropPolygon.TL, cropPolygon.BL);
  var right = distance(cropPolygon.TR, cropPolygon.BR);

  return {
    width: (top + bottom) / 2,
    height: (left + right) / 2,
  };

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: crop an image following a polygon shaped structure
  ARGUMENTS: (
    !img <Image>,
    !canvasSource <canvas dom element>,
    !canvasDestination <canvas dom element>,
    !cropPolygon <{ x: <number>, y: <number>, }> «
      the list of points the cropping will follow
      this list can be in any order, the ordering will be figured out here
      if more than four points, only the first four will be used
    »,
  )
  RETURN: <void>
  NOTE:
    the code from this function and the unwarp one was written with strong inspiration from the following stackoverflow question: https://stackoverflow.com/questions/30565987/cropping-images-with-html5-canvas-in-a-non-rectangular-shape-and-transforming
    special thanks to @markE for the inspiration (https://stackoverflow.com/users/411591/marke)
    the mapTriangle method was already taken by @markE from another stackoverflow question, this is mentionned below by the comment preceding it
*/
module.exports = function (img, canvasSource, canvasDestination, cropPolygon) {

  // anchors defining the warped rectangle
  var croppingPolygon = getCroppingPolygon(cropPolygon);

  // figure out the dimensions that the destination image should have
  var destinationImageDimensions = figureOutDestinationImageDimensions(croppingPolygon);

  // cornerpoints defining the desire unwarped rectangle
  var resultImagePolygon = {
    TL: { x: 0, y: 0, },
    TR: { x: destinationImageDimensions.width, y: 0, },
    BR: { x: destinationImageDimensions.width, y: destinationImageDimensions.height, },
    BL: { x: 0, y: destinationImageDimensions.height, },
  }

  // set size of the destination canvas
  canvasDestination.width = destinationImageDimensions.width;
  canvasDestination.height = destinationImageDimensions.height;

  unwarp(img, canvasDestination, croppingPolygon, resultImagePolygon);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  UNWARP
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: unwarp the given image and draw it in a canvas
  ARGUMENTS: (
    !img <Image> « the image to unwarp »,
    !canvas <canvas dom element> « the canvas in which to draw the unwarped image »,
    !croppingPolygon <getCroppingPolygon·result>,
    !resultImagePolygon <getCroppingPolygon·result>,
  )
  RETURN: <void>
*/
function unwarp (img, canvas, croppingPolygon, resultImagePolygon) {

  // get canvas context
  var context = canvas.getContext("2d");

  // clear the destination canvas
  context.clearRect(0, 0, context.canvas.width, context.canvas.height);

  // unwarp the bottom-left triangle of the warped polygon
  mapTriangle(img, context,
    croppingPolygon.TL,  croppingPolygon.BR,  croppingPolygon.BL,
    resultImagePolygon.TL, resultImagePolygon.BR, resultImagePolygon.BL
  );

  // eliminate slight space between triangles
  context.translate(-1,1);

  // unwarp the top-right triangle of the warped polygon
  mapTriangle(img, context,
    croppingPolygon.TL,  croppingPolygon.TR,  croppingPolygon.BR,
    resultImagePolygon.TL, resultImagePolygon.TR, resultImagePolygon.BR
  );

};

// Perspective mapping: Map warped triangle into unwarped triangle
// Attribution: (SO user: 6502), http://stackoverflow.com/questions/4774172/image-manipulation-and-texture-mapping-using-html5-canvas/4774298#4774298
function mapTriangle (img, ctx, p0, p1, p2, p_0, p_1, p_2) {

  // break out the individual triangles x's & y's
  var x0=p_0.x, y0=p_0.y;
  var x1=p_1.x, y1=p_1.y;
  var x2=p_2.x, y2=p_2.y;
  var u0=p0.x,  v0=p0.y;
  var u1=p1.x,  v1=p1.y;
  var u2=p2.x,  v2=p2.y;

  // save the unclipped & untransformed destination canvas
  ctx.save();

  // clip the destination canvas to the unwarped destination triangle
  ctx.beginPath();
  ctx.moveTo(x0, y0);
  ctx.lineTo(x1, y1);
  ctx.lineTo(x2, y2);
  ctx.closePath();
  ctx.clip();

  // Compute matrix transform
  var delta   = u0 * v1 + v0 * u2 + u1 * v2 - v1 * u2 - v0 * u1 - u0 * v2;
  var delta_a = x0 * v1 + v0 * x2 + x1 * v2 - v1 * x2 - v0 * x1 - x0 * v2;
  var delta_b = u0 * x1 + x0 * u2 + u1 * x2 - x1 * u2 - x0 * u1 - u0 * x2;
  var delta_c = u0 * v1 * x2 + v0 * x1 * u2 + x0 * u1 * v2 - x0 * v1 * u2 - v0 * u1 * x2 - u0 * x1 * v2;
  var delta_d = y0 * v1 + v0 * y2 + y1 * v2 - v1 * y2 - v0 * y1 - y0 * v2;
  var delta_e = u0 * y1 + y0 * u2 + u1 * y2 - y1 * u2 - y0 * u1 - u0 * y2;
  var delta_f = u0 * v1 * y2 + v0 * y1 * u2 + y0 * u1 * v2 - y0 * v1 * u2 - v0 * u1 * y2 - u0 * y1 * v2;

  // Draw the transformed image
  ctx.transform(
    delta_a / delta, delta_d / delta,
    delta_b / delta, delta_e / delta,
    delta_c / delta, delta_f / delta
  );

  // draw the transformed source image to the destination canvas
  ctx.drawImage(img, 0, 0);

  // restore the context to it's unclipped untransformed state
  ctx.restore();

};

var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DRAW IMAGE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display an image like cover would do it with css
  ARGUMENTS: (
    context <canvas context>,
    img <Image>,
  )
  RETURN: <void>
*/
function drawScaledImage (context, img) {

   var canvas = context.canvas;

   // calculate ratio
   var verticalRatio = canvas.height / img.height;
   var horizontalRatio = canvas.width / img.width;
   var ratio  = Math.min(verticalRatio, horizontalRatio);

   // calculate positions
   var x = (canvas.width - img.width*ratio) / 2;
   var y = (canvas.height - img.height*ratio) / 2;

   // draw image
   context.drawImage(
     img,
     0, 0, img.width,       img.height,
     x, y, img.width*ratio, img.height*ratio
   );

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var canvasTools = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CREATE CANVAS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: create an empty canvas
    ARGUMENTS: ( $container <yquerjObject> )
    RETURN: <yquerjObject> « the created canvas »
  */
  create: function ($container) {

    var $canvas = $container.canva();
    var canvas = $canvas[0];
    var context2D = canvas.getContext("2d");

    // absolute position for canvas
    $canvas.css("position", "absolute");

    // add loading text
    context2D.fillStyle = "black";
    context2D.font = "bold 16px Arial";
    context2D.fillText("Loading image...", 10, (canvas.height / 2) + 8);

    // scale canvas appropriately
    canvasTools.scaleCanvasFull($canvas);

    return $canvas;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY IMAGE IN CANVAS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: display the given image in canvas
    ARGUMENTS: (
      !$canvas <yquerjObject> « the canvas in which to display the image »,
      !$drawCanvas <yquerjObject> « the parallelle canvas to draw on (just so it's rightly scaled) »,
      !fileOrUrl <string|File> « the url or file to display »,
    )
    RETURN: <Image>
  */
  displayImage: function ($canvas, $drawCanvas, fileOrUrl) {

    var canvas = $canvas[0];
    var drawCanvas = $drawCanvas[0];
    var context2D = canvas.getContext("2d");

    // add image
    var img = new Image();
    img.onload = function () {

      // set both canvas dimensions to the ones of the image
      canvas.width = drawCanvas.width = img.width;
      canvas.height = drawCanvas.height = img.height;

      // scale canvas appropriately
      canvasTools.scaleCanvasFull($canvas);
      canvasTools.scaleCanvasFull($drawCanvas);

      // cleanup canvas
      context2D.clearRect(0, 0, canvas.width, canvas.height);

      // show image
      context2D.drawImage(img, 0, 0);

    };

    // set image source from url or file
    if (_.isString(fileOrUrl)) img.src = fileOrUrl
    else img.src = URL.createObjectURL(fileOrUrl);

    // figure out image type
    if (_.isString(fileOrUrl)) img.imageType = fileOrUrl.match(/\.png$/) ? "image/png" : "image/jpg"
    else img.imageType = fileOrUrl.type;

    // return the created image object
    return img;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SCALE AND CENTER CANVAS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: scale a canvas for it to fill it's parent "div", also centers it (something like what css "cover" achieves)
    ARGUMENTS: ( $canvas <yquerjObject> « canvas to scale » )
    RETURN: <void>
  */
  scaleCanvasFull: function ($canvas) {

    var $parent = $canvas.parent();

    // try width 100%
    $canvas.css("width", "100%");
    $canvas.css("height", "");

    // if content too high, use height 100% instead
    if ($canvas.height() > $parent.height()) {
      $canvas.css("width", "");
      $canvas.css("height", "100%");
      // center content horizontally
      $canvas.css("top", 0);
      $canvas.css("left", ($parent.width() - $canvas.width()) / 2);
    }
    else {
      // center content vertically
      $canvas.css("left", 0);
      $canvas.css("top", ($parent.height() - $canvas.height()) / 2);
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = canvasTools;

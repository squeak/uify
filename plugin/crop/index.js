var _ = require("underscore");
var $$ = require("squeak");
var uify = require("../../core");
var checkFile = require("./checkFile");
var displayDialog = require("./dialog");
var canvasTools = require("./canvas");
var nonRectangular = require("./nonRectangular");
var rectangular = require("./rectangular");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display a dialog the user can use to setup and crop an image
  ARGUMENTS: ({
    ?title: <string>,
    ?subtitle: <string>,
    !fileOrUrl: <File|string>,
    !interfaceMaker: <function(
      $drawSpace <yquerjObject>,
      $originCanvas <yquerjObject>,
    )>,
    !cropProcessing: <function({
      !img <Image>,
      !canvasSource: <dom canvas element>,
      !canvasDestination: <dom canvas element>,
    })>,
    !saveCallback: <function(blob<Blob>)> « exectued when cropped done and approved »,
  })
  RETURN: <void>
*/
function crop (options) {

  var img, $originCanvas, $destinationCanvas, drawInterface, croppingPolygon;

  checkFile(options.fileOrUrl, function () {
    displayDialog({
      title: options.title,
      subtitle: options.subtitle,
      displayedCallback: function (drawSpace, visualizeSpace) {

        // DISPLAY ORIGINAL IMAGE
        $originCanvas = canvasTools.create(drawSpace.$el);
        $drawCanvas = canvasTools.create(drawSpace.$el);
        img = canvasTools.displayImage($originCanvas, $drawCanvas, options.fileOrUrl);

        // DISPLAY DRAWING CANVAS
        drawInterface = options.interfaceMaker($drawCanvas, function (croppingPointsList) { croppingPolygon = croppingPointsList; });

        // DISPLAY PROCESSED IMAGE'S CANVAS
        $destinationCanvas = canvasTools.create(visualizeSpace.$el);

      },
      selectFull: function () {
        drawInterface.pointsList = [
          { x: 0, y: 0, },
          { x: drawInterface.canvas.width, y: drawInterface.canvas.height, },
        ];
        drawInterface.draw();
      },
      clearInterface: function () { drawInterface.clear(); },
      readyToCrop: function () {
        if (drawInterface.pointsList.length >= options.minimumNumberOfPointsToProcess) return true
        else {
          alert("You should select at least "+ options.minimumNumberOfPointsToProcess +" points before croping.")
          return false;
        };
      },
      cropProcess: function () {
        options.cropProcessing(img, $originCanvas[0], $destinationCanvas[0], croppingPolygon);
        canvasTools.scaleCanvasFull($destinationCanvas);
      },
      save: function () {
        if (img.imageType == "image/png") $destinationCanvas[0].toBlob(options.saveCallback)
        else $destinationCanvas[0].toBlob(options.saveCallback, "image/jpeg");
      },
    });
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE DIFFERENT TYPE OF CROPS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var makeCrop = {

  //
  //                              RECTANGULAR CROP

  rectangular: function (fileOrUrl, ratio, finalCallback) {
    crop({
      title: "Crop an image",
      fileOrUrl: fileOrUrl,
      minimumNumberOfPointsToProcess: 2,
      ratio: ratio,
      interfaceMaker: _.partial(rectangular.interface, ratio),
      cropProcessing: rectangular.processing,
      saveCallback: finalCallback,
    });
  },

  //
  //                              NON RECTANGULAR CROP

  free: function (fileOrUrl, ratio, finalCallback) {
    crop({
      title: "Crop an image drawing a non rectangular frame",
      subtitle: "(don't expect wonderful results if you try shapes that are too weird)",
      fileOrUrl: fileOrUrl,
      minimumNumberOfPointsToProcess: 4,
      interfaceMaker: nonRectangular.interface,
      cropProcessing: nonRectangular.processing,
      saveCallback: finalCallback,
    });
  },

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: display a dialog to crop the given image
  ARGUMENTS: ({
    !image <File|Blob|string> « the file or url to the file to crop »,
    ?shape: <"rectangular"|"free">@default="rectangular" « what shape cropping can user, rectangular, non rectangular for edges shape... »,
    ?ratio: <number> « if rectangular shape, you can constraint the ratio of the crop with this option »,
    !callback <function(blob<Blob>)> « executed when cropped successfully »
  })
  RETURN: <void>
*/
uify.crop = function (options) {

  var defaultOptions = {
    shape: "rectangular",
  };
  options = $$.defaults(defaultOptions, options);

  if (!makeCrop[options.shape]) $$.log.detailedError(
    "uify.crop",
    "Unknown crop shape: ",
    options.shape,
    "Full options you passed: ",
    options
  );
  else return makeCrop[options.shape](options.image, options.ratio, options.callback);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = uify.crop;

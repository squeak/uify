var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (fileOrUrl, callback) {

  // TODO: could be nice to check file type for urls as well

  // if the file is not an image: alert
  if (!_.isString(fileOrUrl) && fileOrUrl.type !== "" && !fileOrUrl.type.match("image.*")) $$.alert.detailedError(
    "descartes/inputifiy.fileProcess.nonRectangularCrop",
    "Cannot crop a non image file."
  )

  // else go ahead
  else callback();

};

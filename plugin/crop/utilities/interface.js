var _ = require("underscore");
var $$ = require("squeak");
var distance = require("./distance");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  VARIABLES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var lineThicknessFactor = 3;
var circleRadiusFactor = 40;

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  HANDLE MOUSE EVENTS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function handleMouseEvents (workZone, clickedSurfaceCallback, clickedPointCallback, movePointCallback) {

  //
  //                              MOUSE DOWN

  workZone.$canvas.mousedown(function (e) {

    e.preventDefault();
    e.stopPropagation();

    // GET CLICK COORDINATES
    var clickCoordinates = workZone.getClickCoordinates(e);

    // GET CURRENTLY CLICKED POINT (OR NULL IF NONE IS CLICKED)
    workZone.currentlyClickedPoint = getClickedPoint(workZone.pointsList, clickCoordinates, workZone.getCircleScaledRadius());

    // CALLBACK CLICKED POINT OR SURFACE
    if (workZone.currentlyClickedPoint) { if (clickedPointCallback) clickedPointCallback(e, clickCoordinates); }
    else if (clickedSurfaceCallback) clickedSurfaceCallback(e, clickCoordinates);

  });

  //
  //                              MOUSE MOVE

  workZone.$canvas.mousemove(function (e) {
    // MOVE POINT
    if (workZone.currentlyClickedPoint && movePointCallback) movePointCallback(e, workZone.getClickCoordinates(e));
  });

  //
  //                              MOUSE UP

  workZone.$canvas.mouseup(function (e) {
    // UNSET CURRENTLY SELECTED POINT
    workZone.currentlyClickedPoint = null;
  });

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  IS CLICKING POINT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function getClickedPoint (pointsList, clickCoordinates, circleScaledRadius) {
  return _.find(pointsList, function (coordinates) {
    return distance(clickCoordinates, coordinates) <= circleScaledRadius;
  });
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
  ARGUMENTS: (
    !$canvas,
    !shapeChanged <function(croppingPointsList « list of points of the polygon »)>,
    !draw <function(workZone)>,
    ?clickedPoint <function(e, clickCoordinates)>,
    ?clickedSurface <function(e, clickCoordinates)>,
    ?movePoint <function(e, clickCoordinates)>,
  )
  RETURN:
*/
module.exports = function (options) {

  var workZone = {
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    // GENERAL ELEMENTS
    $canvas: options.$canvas,
    canvas: options.$canvas[0],
    context: options.$canvas[0].getContext("2d"),
    pointsList: [],
    currentlyClickedPoint: null,

    // GET IMAGE SCALE FACTOR (THE ONE CAUSED BY CSS STYLING)
    getScale: function () { return workZone.canvas.width / workZone.canvas.offsetWidth; },
    getLineThickness: function () { return lineThicknessFactor * workZone.getScale(); },
    getCircleScaledRadius: function () { return circleRadiusFactor * workZone.getScale(); },

    // GET SCALED COORDINATES OF A CLICK EVENT
    getClickCoordinates: function (e) {
      return {
        x: e.originalEvent.layerX * workZone.getScale(),
        y: e.originalEvent.layerY * workZone.getScale(),
      };
    },

    // CLEAR CANVAS
    clear: function () {
      workZone.pointsList = [];
      workZone.context.clearRect(0, 0, workZone.canvas.width, workZone.canvas.height);
      options.shapeChanged(workZone.pointsList);
    },

    // (RE)DRAW CANVAS
    draw: function (e) {

      // clear canvas
      workZone.context.clearRect(0, 0, workZone.canvas.width, workZone.canvas.height);

      // draw shape on canvas
      options.draw(workZone, e || {});

      // callback list of points
      options.shapeChanged(workZone.pointsList);

    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  };

  // MOUS EVENTS HANDLING
  handleMouseEvents(workZone, options.clickedSurface, options.clickedPoint, options.movePoint);

  return workZone;
};
